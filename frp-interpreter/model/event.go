package model

import (
	"encoding/json"
	"time"

	"github.com/google/uuid"
)

type Event struct {
	ID           uuid.UUID       `json:"id"`
	RegisteredAt time.Time       `json:"registeredAt"`
	OccurredAt   time.Time       `json:"occurredAt"`
	SubjectIDs   []uuid.UUID     `json:"subjectIds"`
	EventType    string          `json:"eventType"`
	EventData    json.RawMessage `json:"eventData"`
}

// BirthEventData is used to decode events from the seeder and simulator
type BirthEventData struct {
	Birth struct {
		DateOfBirth  string `json:"dateOfBirth"`
		PlaceOfBirth struct {
			Code string `json:"code"`
			Name string `json:"name"`
		} `json:"placeOfBirth"`
		CountryOfBirth struct {
			Code string `json:"code"`
			Name string `json:"name"`
		} `json:"countryOfBirth"`
	} `json:"birth"`
	BSN    int    `json:"bsn"`
	Gender string `json:"gender"`
	Naming struct {
		FirstName  string `json:"firstName"`
		Prefix     string `json:"prefix"`
		FamilyName string `json:"familyName"`
	} `json:"naming"`
	AddressRegistration struct {
		AddressID uuid.UUID `json:"address"`
	} `json:"addressRegistration"`
}

// NameChangeEventData is used to decode events from the simulator
type NameChangeEventData struct {
	FirstName  string `json:"firstName"`
	Prefix     string `json:"prefix"`
	FamilyName string `json:"familyName"`
}

type Municipality struct {
	Code string `json:"code"`
	Name string `json:"name"`
}

type AddressRegistrationEventData struct {
	Street              string       `json:"street"`
	HouseNumber         int          `json:"houseNumber"`
	HouseNumberAddition string       `json:"houseNumberAddition"`
	HouseLetter         string       `json:"houseLetter"`
	PostalCode          string       `json:"postalCode"`
	Municipality        Municipality `json:"municipality"`
	Surface             int          `json:"surface"`
	Purpose             string       `json:"purpose"`
}
