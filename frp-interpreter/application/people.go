package application

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/google/uuid"
	claimAPI "gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/claim-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/frp-interpreter/model"
)

var claimantID = uuid.UUID{} // TODO: claimant ID of the simulator/interpreter

var genderMapping = map[string]string{
	"M": "M",
	"F": "V",
	"X": "X",
}

func (app *Application) HandleBirthEvent(ctx context.Context, event *model.Event) error {
	data := new(model.BirthEventData)
	if err := json.Unmarshal(event.EventData, data); err != nil {
		return fmt.Errorf("json unmarshal failed: %w", err)
	}

	// Create several claims based on the birth registration event
	if len(event.SubjectIDs) == 0 {
		return fmt.Errorf("no subject ID in event")
	}

	subjectID := event.SubjectIDs[0]

	// Create the Burgerservicenummer claim
	if resp, err := app.client.CreateClaimWithResponse(ctx, claimAPI.CreateClaimJSONRequestBody{
		Input: claimAPI.Claim{
			ClaimType: "Burgerservicenummer",
			ClaimKind: claimAPI.Assertion,
			Claimant:  claimantID,
			Subject:   subjectID,
			Data: claimAPI.ClaimData{
				"value": strconv.Itoa(data.BSN), // Note: convert to string, since the HaalCentraal API considers BSNs to be strings (probably because previously there could be 8 digits with a leading 0)
			},
			RegistrationStart: event.RegisteredAt, // IMPROVE: use time.Now()? Also below
			ValidStart:        &event.OccurredAt,
		},
	}); err != nil {
		return fmt.Errorf("error creating Burgerservicenummer claim: %w", err)
	} else if resp.JSON201 == nil && strings.Contains(string(resp.Body), "unknown claim type") {
		// If the claim types are not yet initialized/created, do this first and retry creating the claim. Note: not doing this during initialization, since 1) when using the 'Reset' endpoint, all claim types are deleted and 2) trying to create claim types that already exist results in errors in the logs
		app.InitEventTypes(ctx)

		// Retry creating the Burgerservicenummer claim
		if _, err := app.client.CreateClaimWithResponse(ctx, claimAPI.CreateClaimJSONRequestBody{
			Input: claimAPI.Claim{
				ClaimType: "Burgerservicenummer",
				ClaimKind: claimAPI.Assertion,
				Claimant:  claimantID,
				Subject:   subjectID,
				Data: claimAPI.ClaimData{
					"value": strconv.Itoa(data.BSN), // Note: convert to string, since the HaalCentraal API considers BSNs to be strings (probably because previously there could be 8 digits with a leading 0)
				},
				RegistrationStart: event.RegisteredAt, // IMPROVE: use time.Now()? Also below
				ValidStart:        &event.OccurredAt,
			},
		}); err != nil {
			return fmt.Errorf("error creating Burgerservicenummer claim: %w", err)
		}
	}

	// Create the Voornamen claim
	if _, err := app.client.CreateClaimWithResponse(ctx, claimAPI.CreateClaimJSONRequestBody{
		Input: claimAPI.Claim{
			ClaimType: "Voornamen",
			ClaimKind: claimAPI.Assertion,
			Claimant:  claimantID,
			Subject:   subjectID,
			Data: claimAPI.ClaimData{
				"value": data.Naming.FirstName,
			},
			RegistrationStart: event.RegisteredAt,
			ValidStart:        &event.OccurredAt,
		},
	}); err != nil {
		return fmt.Errorf("error creating Voornamen claim: %w", err)
	}

	// Create the Voorvoegsel claim
	if _, err := app.client.CreateClaimWithResponse(ctx, claimAPI.CreateClaimJSONRequestBody{
		Input: claimAPI.Claim{
			ClaimType: "Voorvoegsel",
			ClaimKind: claimAPI.Assertion,
			Claimant:  claimantID,
			Subject:   subjectID,
			Data: claimAPI.ClaimData{
				"value": data.Naming.Prefix,
			},
			RegistrationStart: event.RegisteredAt,
			ValidStart:        &event.OccurredAt,
		},
	}); err != nil {
		return fmt.Errorf("error creating Voorvoegsel claim: %w", err)
	}

	// Create the Geslachtsnaam claim
	if _, err := app.client.CreateClaimWithResponse(ctx, claimAPI.CreateClaimJSONRequestBody{
		Input: claimAPI.Claim{
			ClaimType: "Geslachtsnaam",
			ClaimKind: claimAPI.Assertion,
			Claimant:  claimantID,
			Subject:   subjectID,
			Data: claimAPI.ClaimData{
				"value": data.Naming.FamilyName,
			},
			RegistrationStart: event.RegisteredAt,
			ValidStart:        &event.OccurredAt,
		},
	}); err != nil {
		return fmt.Errorf("error creating Geslachtsnaam claim: %w", err)
	}

	// Create the Geboortedatum claim
	if _, err := app.client.CreateClaimWithResponse(ctx, claimAPI.CreateClaimJSONRequestBody{
		Input: claimAPI.Claim{
			ClaimType: "Geboortedatum",
			ClaimKind: claimAPI.Assertion,
			Claimant:  claimantID,
			Subject:   subjectID,
			Data: claimAPI.ClaimData{
				"value": data.Birth.DateOfBirth,
			},
			RegistrationStart: event.RegisteredAt,
			ValidStart:        &event.OccurredAt,
		},
	}); err != nil {
		return fmt.Errorf("error creating Geboortedatum claim: %w", err)
	}

	// Create the Geboorteplaats claim
	if _, err := app.client.CreateClaimWithResponse(ctx, claimAPI.CreateClaimJSONRequestBody{
		Input: claimAPI.Claim{
			ClaimType: "Geboorteplaats",
			ClaimKind: claimAPI.Assertion,
			Claimant:  claimantID,
			Subject:   subjectID,
			Data: claimAPI.ClaimData{
				"code":         data.Birth.PlaceOfBirth.Code, // IMPROVE: store only if nonempty
				"omschrijving": data.Birth.PlaceOfBirth.Name,
			},
			RegistrationStart: event.RegisteredAt,
			ValidStart:        &event.OccurredAt,
		},
	}); err != nil {
		return fmt.Errorf("error creating Geboorteplaats claim: %w", err)
	}

	// Create the Geboorteland claim
	if _, err := app.client.CreateClaimWithResponse(ctx, claimAPI.CreateClaimJSONRequestBody{
		Input: claimAPI.Claim{
			ClaimType: "Geboorteland",
			ClaimKind: claimAPI.Assertion,
			Claimant:  claimantID,
			Subject:   subjectID,
			Data: claimAPI.ClaimData{
				"code":         data.Birth.CountryOfBirth.Code,
				"omschrijving": data.Birth.CountryOfBirth.Name,
			},
			RegistrationStart: event.RegisteredAt,
			ValidStart:        &event.OccurredAt,
		},
	}); err != nil {
		return fmt.Errorf("error creating Geboorteland claim: %w", err)
	}

	// Create the Geslacht claim
	if _, err := app.client.CreateClaimWithResponse(ctx, claimAPI.CreateClaimJSONRequestBody{
		Input: claimAPI.Claim{
			ClaimType: "Geslacht",
			ClaimKind: claimAPI.Assertion,
			Claimant:  claimantID,
			Subject:   subjectID,
			Data: claimAPI.ClaimData{
				"value": genderMapping[data.Gender],
			},
			RegistrationStart: event.RegisteredAt,
			ValidStart:        &event.OccurredAt,
		},
	}); err != nil {
		return fmt.Errorf("error creating Geslacht claim: %w", err)
	}

	// Create the Adressering claim
	if address, ok := addresses[data.AddressRegistration.AddressID]; ok {
		if _, err := app.client.CreateClaimWithResponse(ctx, claimAPI.CreateClaimJSONRequestBody{
			Input: claimAPI.Claim{
				ClaimType: "Adressering",
				ClaimKind: claimAPI.Assertion,
				Claimant:  claimantID,
				Subject:   subjectID,
				Data: claimAPI.ClaimData{
					"street":              address.Street,
					"houseNumber":         address.HouseNumber,
					"houseNumberAddition": address.HouseNumberAddition,
					"houseLetter":         address.HouseLetter,
					"postalCode":          address.PostalCode,
					"municipality":        address.Municipality,
					"purpose":             address.Purpose,
					"surface":             address.Surface,
				},
				RegistrationStart: event.RegisteredAt,
				ValidStart:        &event.OccurredAt,
			},
		}); err != nil {
			return fmt.Errorf("error creating Geslacht claim: %w", err)
		}
	}

	fmt.Println("created claims for GeboorteVastgesteld event")

	return nil
}

func (app *Application) HandleNameChangeEvent(ctx context.Context, event *model.Event) error {
	data := new(model.NameChangeEventData)
	if err := json.Unmarshal(event.EventData, data); err != nil {
		return fmt.Errorf("json unmarshal failed: %w", err)
	}

	// Create several claims based on the birth registration event
	if len(event.SubjectIDs) == 0 {
		return fmt.Errorf("no subject ID in event")
	}

	subjectID := event.SubjectIDs[0]

	// IMPROVE: fetch the existing name first and only update the changed fields? Or let the simulator send this data / send only changed fields?

	// Create the Voornamen claim
	if _, err := app.client.CreateClaimWithResponse(ctx, claimAPI.CreateClaimJSONRequestBody{
		Input: claimAPI.Claim{
			ClaimType: "Voornamen",
			ClaimKind: claimAPI.Assertion,
			Claimant:  claimantID,
			Subject:   subjectID,
			Data: claimAPI.ClaimData{
				"value": data.FirstName,
			},
			RegistrationStart: event.RegisteredAt,
			ValidStart:        &event.OccurredAt,
		},
	}); err != nil {
		return fmt.Errorf("error creating Voornamen claim: %w", err)
	}

	// Create the Voorvoegsel claim
	if _, err := app.client.CreateClaimWithResponse(ctx, claimAPI.CreateClaimJSONRequestBody{
		Input: claimAPI.Claim{
			ClaimType: "Voorvoegsel",
			ClaimKind: claimAPI.Assertion,
			Claimant:  claimantID,
			Subject:   subjectID,
			Data: claimAPI.ClaimData{
				"value": data.Prefix,
			},
			RegistrationStart: event.RegisteredAt,
			ValidStart:        &event.OccurredAt,
		},
	}); err != nil {
		return fmt.Errorf("error creating Voorvoegsel claim: %w", err)
	}

	// Create the Geslachtsnaam claim
	if _, err := app.client.CreateClaimWithResponse(ctx, claimAPI.CreateClaimJSONRequestBody{
		Input: claimAPI.Claim{
			ClaimType: "Geslachtsnaam",
			ClaimKind: claimAPI.Assertion,
			Claimant:  claimantID,
			Subject:   subjectID,
			Data: claimAPI.ClaimData{
				"value": data.FamilyName,
			},
			RegistrationStart: event.RegisteredAt,
			ValidStart:        &event.OccurredAt,
		},
	}); err != nil {
		return fmt.Errorf("error creating Geslachtsnaam claim: %w", err)
	}

	fmt.Println("created claims for NamenGewijzigd event")

	return nil
}

// func (app *Application) HandleNamingEvent(ctx context.Context, event *model.Event) error {
// 	data := new(model.NamingEventData)
// 	if err := json.Unmarshal(event.EventData, data); err != nil {
// 		return fmt.Errorf("json unmarshal failed: %w", err)
// 	}

// 	if app.Filter(data.Municipality.Code) {
// 		return nil
// 	}

// 	namingEvent := model.NamingEvent{
// 		Name: fmt.Sprintf("%s %s", data.GivenName, data.Surname),
// 	}

// 	if err := app.Request(ctx, http.MethodPatch, fmt.Sprintf("/people/%s", event.SubjectIDs[0].String()), namingEvent, nil); err != nil {
// 		return fmt.Errorf("request failed: %w", err)
// 	}

// 	return nil
// }

// func (app *Application) HandleDeathEvent(ctx context.Context, event *model.Event) error {
// 	deathEvent := model.DeathEvent{
// 		DiedAt: event.OccurredAt,
// 	}

// 	if err := app.Request(ctx, http.MethodPatch, fmt.Sprintf("/people/%s", event.SubjectIDs[0]), deathEvent, nil); err != nil {
// 		return fmt.Errorf("request failed: %w", err)
// 	}

// 	return nil
// }

// func (app *Application) HandleRegisteredOnAddressEvent(ctx context.Context, event *model.Event) error {
// 	data := new(model.RegisteredOnAddressEventData)
// 	if err := json.Unmarshal(event.EventData, data); err != nil {
// 		return fmt.Errorf("json unmarshal failed: %w", err)
// 	}

// 	if app.Filter(data.Municipality.Code) {
// 		return nil
// 	}

// 	addressRegisteredEvent := model.AddressChangedEvent{
// 		RegisterAddressID: data.AddressID,
// 	}

// 	if err := app.Request(ctx, http.MethodPatch, fmt.Sprintf("/people/%s", event.SubjectIDs[0]), addressRegisteredEvent, nil); err != nil {
// 		return fmt.Errorf("request failed: %w", err)
// 	}

// 	return nil
// }

// func (app *Application) HandleAddressDeregisteredEvent(ctx context.Context, event *model.Event) error {
// 	addressDeregisteredEvent := model.AddressChangedEvent{
// 		RegisterAddressID: uuid.UUID{},
// 	}

// 	if err := app.Request(ctx, http.MethodPatch, fmt.Sprintf("/people/%s", event.SubjectIDs[0]), addressDeregisteredEvent, nil); err != nil {
// 		return fmt.Errorf("request failed: %w", err)
// 	}

// 	return nil
// }
