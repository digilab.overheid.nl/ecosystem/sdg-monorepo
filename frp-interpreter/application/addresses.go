package application

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/frp-interpreter/model"
)

// Store in map of address ID to municipality + code
var addresses = make(map[uuid.UUID]*model.AddressRegistrationEventData)

// Handle address registration events
func (app *Application) HandleAddressRegistrationEvent(ctx context.Context, event *model.Event) error {
	// Unmarshal the event data
	data := new(model.AddressRegistrationEventData)
	if err := json.Unmarshal(event.EventData, data); err != nil {
		return fmt.Errorf("json unmarshal failed: %w", err)
	}

	if len(event.SubjectIDs) != 0 {
		addresses[event.SubjectIDs[0]] = data
	}

	return nil
}
