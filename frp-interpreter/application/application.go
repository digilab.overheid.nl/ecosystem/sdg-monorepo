package application

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"

	"github.com/nats-io/nats.go"
	claimAPI "gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/claim-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/frp-interpreter/model"
)

type Application struct {
	client     *claimAPI.ClientWithResponses
	baseURL    string
	httpClient *http.Client
}

var ErrResponseFailed = errors.New("response failed with")

func NewApplication(baseURL string) *Application {
	client, err := claimAPI.NewClientWithResponses(baseURL)
	if err != nil {
		log.Fatal("error creating application", err)
	}

	return &Application{
		client:     client,
		baseURL:    baseURL,
		httpClient: http.DefaultClient,
	}
}

// InitEventTypes creates all event types that are required by the interpreter
func (app *Application) InitEventTypes(ctx context.Context) {
	fmt.Println("creating claim types")

	for _, eventTypeName := range []string{
		"Geboortedatum",
		"Geboorteplaats",
		"Geslacht",
		"Geslachtsnaam",
		"Voornamen",
		"Geboorteland",
		"Voorvoegsel",
		"Burgerservicenummer",
		"Adressering",
	} {
		app.client.CreateClaimTypeWithResponse(ctx, claimAPI.CreateClaimTypeJSONRequestBody{
			Input: claimAPI.ClaimType{
				Name:   eventTypeName,
				Schema: map[string]interface{}{}, // Note: for now, the backend ignores the schema, so no need to set this (other than nil, which is refused). IMPROVE: add a schema for each event type
				// ValidEnd: &claimAPI.Date{},
				// ValidStart: &claimAPI.Date{},
			},
		})
		// if err != nil {
		// 	fmt.Printf("warning during claim type initialization: %v\n", err)
		// }

		// if resp.JSON201 == nil {
		// 	fmt.Printf("warning in response during claim type initialization: %s\n", resp.Body)
		// }

		// Note: we ignore the result and possible error. E.g. in case event types already exist, an error is returned
	}
}

func (app *Application) HandleCommands(msg *nats.Msg) {
	event := string(msg.Data)

	fmt.Printf("HANDLING: %s \n", event)

	switch event {
	case "reset":
		req, err := http.NewRequest(http.MethodDelete, fmt.Sprintf("%s/reset", app.baseURL), nil)
		if err != nil {
			fmt.Println(fmt.Errorf("new request failed: %w", err))
			return
		}

		resp, err := app.httpClient.Do(req)
		if err != nil {
			fmt.Println(fmt.Errorf("new request failed: %w", err))
			return
		}

		if resp.StatusCode != http.StatusOK {
			b, _ := httputil.DumpRequestOut(req, true)

			fmt.Println(fmt.Errorf("request failed: %s%s", string(b), resp.Status))
			return
		}
	}
}

func (app *Application) HandleEvents(msg *nats.Msg) {
	event := new(model.Event)
	if err := json.Unmarshal(msg.Data, event); err != nil {
		fmt.Print(fmt.Errorf("unmarshal failed: %w", err))
		return
	}

	switch event.EventType {
	case "GeboorteVastgesteld":
		if err := app.HandleBirthEvent(context.Background(), event); err != nil {
			fmt.Println(fmt.Errorf("handle birth event failed: %w", err))
		}

	case "NamenGewijzigd":
		if err := app.HandleNameChangeEvent(context.Background(), event); err != nil {
			fmt.Println(fmt.Errorf("handle name change event failed: %w", err))
		}

	case "AdresGeregistreerd":
		if err := app.HandleAddressRegistrationEvent(context.Background(), event); err != nil {
			fmt.Println(fmt.Errorf("handle naming event failed: %w", err))
		}

	// case "NamenVastgesteld":
	// 	if err := app.HandleNamingEvent(context.Background(), event); err != nil {
	// 		fmt.Println(fmt.Errorf("handle naming event failed: %w", err))
	// 	}

	// case "OverlijdenVastgesteld":
	// 	if err := app.HandleDeathEvent(context.Background(), event); err != nil {
	// 		fmt.Println(fmt.Errorf("handle death event failed: %w", err))
	// 	}

	// case "AdresIngeschreven":
	// 	if err := app.HandleRegisteredOnAddressEvent(context.Background(), event); err != nil {
	// 		fmt.Println(fmt.Errorf("handle address registered event failed: %w", err))
	// 	}

	// case "AdresUitgeschreven":
	// 	if err := app.HandleAddressDeregisteredEvent(context.Background(), event); err != nil {
	// 		fmt.Println(fmt.Errorf("handle address written out event failed: %w", err))
	// 	}

	default:
		fmt.Printf("Event type: %s unknown\n", event.EventType)
	}
}
