---
title : "Synthetic Data Generator (SDG) for Fictional Registration of Persons (FRP)"
description: "Documentation for the SDG with a specific implemenation to create data for the FRP"
lead: ""
date: 2023-08-28T14:52:40+02:00
draft: true
toc: true
---

## Running locally
Clone [this repo](https://gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo).

To run the k3d development cluster run:

Run:
```shell
make dev
```

### Required programs
Ensure the following programs are installed to run the cluster:
- golang
- k3d
- kubectl
- kustomize
- skaffold

### Additional Development Tools
For development purposes, you will also need:
- golangci-lint
- jq
- pre-commit
- oapi-codegen

All versions for the specificied programs can be found [here](.tool_versions).
All tools and versions are managed by asdf.

By running ```shell asdf install``` all tooling and the correct versions are installed.
