# Generate Kubernetes manifests from Helm chart

`nats` is published as a public Helm chart. We use it to generate the manifests and split the manifests. You can split with [yq](https://github.com/mikefarah/yq) or [kubectl slice](https://github.com/patrickdappollonio/kubectl-slice).

```console
# cleanup
rm *.yml

# generate manifests and split with yq
helm template sdg-nats nats/nats \
  --version=1.1.11 \
  --values=helm/values.yaml \
  | yq --split-exp='(.kind | downcase) + "-" + .metadata.name'

# OR generate manifests and split with kubectl slice
helm template sdg-nats nats/nats \
  --version=1.1.11 \
  --values=helm/values.yaml \
  | kubectl slice --output-dir=. -

# generate kustomization file
kustomize create --autodetect
```
