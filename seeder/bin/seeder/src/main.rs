use std::net::SocketAddr;
use std::time::Duration;

use async_nats::jetstream;
use axum::{
    routing::{get, post},
    Router,
};
use clap::Parser;
use tracing::info;
use url::Url;

mod config;
mod errors;
mod handler;
mod logger;
use config::Config;
use errors::AppError;
use handler::{health_check, seed_realm, AppState};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    /// Socket to bind API server to.
    #[arg(long, default_value = "0.0.0.0:8080")]
    bind_addr: SocketAddr,

    /// NATS endpoint to publish to.
    #[arg(long, default_value = "nats://nats:4222/events")]
    nats_stream: Url,

    /// Amount of instances to spawn per type of object
    #[arg(long, default_value_t = 100000)]
    num_instances: usize,

    /// Base64 encoded seed to be used by PRNG
    #[arg(long, default_value = None)]
    seed: Option<String>,
}

#[tokio::main]
async fn main() -> Result<(), AppError> {
    logger::init()?;

    info!("Init seeder");

    let config = Config::new(Args::parse());

    info!("Starting nats connection");
    let connection = async_nats::ConnectOptions::new()
        .flush_interval(Duration::from_millis(100))
        .connect(config.nats_stream.to_string())
        .await?;

    info!("Starting jetstream connection");
    let stream = jetstream::new(connection.clone());

    info!("Starting app");
    let state = AppState::new(
        realm::Config {
            num_instances: config.num_instances,
            seed: config.seed,
        },
        stream,
    );

    info!("Starting router");
    let app = Router::new()
        .route("/health", get(health_check))
        .route("/seed", post(seed_realm))
        .layer(logger::LoggerLayer)
        .with_state(state);

    Ok(axum::Server::bind(&config.bind_addr)
        .serve(app.into_make_service())
        .await?)
}
