use async_nats::jetstream;
use axum::extract::{Json, State};
use serde::Deserialize;
use tracing::info;

use crate::AppError;

#[derive(Clone)]
pub struct AppState {
    pub config: realm::Config,
    pub stream: jetstream::Context,
}

impl AppState {
    pub fn new(config: realm::Config, stream: jetstream::Context) -> Self {
        Self { config, stream }
    }
}

#[derive(Deserialize)]
pub struct RequestParams {
    pub num_instances: Option<usize>,
    pub seed: Option<String>,
}

pub async fn publish_events(
    config: realm::Config,
    stream: jetstream::Context,
) -> Result<(), AppError> {
    info!("generating seed");

    for event in realm::Genesis::new(config) {
        let encoded_event = serde_json::to_string(&event).expect("unserializable event");

        let subject = match event.event_type.as_str() {
            "AdresGeregistreerd" => "events.frag.adres.Geregistreerd",
            "GeboorteVastgesteld" => "events.frp.persoon.GeboorteVastgesteld",
            // "BSNVastgesteld" => "events.frp.persoon.BSNVastgesteld",
            // "GeslachtVastgesteld" => "events.frp.persoon.GeslachtVastgesteld",
            // "NamenVastgesteld" => "events.frp.persoon.NamenVastgesteld",
            // "AdresIngeschreven" => "events.frp.persoon.AdresIngeschreven",
            "PerceelGeregistreerd" => "events.frk.perceel.Geregistreerd",
            "GebouwGeregistreerd" => "events.frag.gebouw.Geregistreerd",
            "AdresToewijzing" => "events.frag.adres.Toewijzing",
            "VoertuigGeregistreerd" => "events.frv.voertuig.Geregistreerd",
            "WozBepaling" => "events.woz.bepaling",
            _ => unimplemented!(),
        };

        stream.publish(subject.into(), encoded_event.into()).await?;
    }

    info!("seed generation has completed");
    Ok(())
}

// Route: GET "/health"
pub async fn health_check<'a>() -> &'a str {
    "OK"
}

// Route: POST "/seed"
pub async fn seed_realm(
    State(state): State<AppState>,
    payload: Json<RequestParams>,
) -> Result<(), AppError> {
    let config = realm::Config {
        num_instances: payload
            .0
            .num_instances
            .unwrap_or(state.config.num_instances),
        seed: payload.0.seed.or(state.config.seed),
    };
    tokio::spawn(publish_events(config, state.stream));
    Ok(())
}
