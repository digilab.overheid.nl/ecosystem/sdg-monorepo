use std::net::SocketAddr;

use url::Url;

use crate::Args;

pub struct Config {
    pub bind_addr: SocketAddr,
    pub seed: Option<String>,
    pub num_instances: usize,
    pub nats_stream: Url,
}

impl Config {
    pub fn new(args: Args) -> Self {
        Self {
            bind_addr: std::env::var("BIND_ADDR")
                .map(|url| url.parse().expect("invalid socket_addr"))
                .unwrap_or(args.bind_addr),
            seed: std::env::var("SEED_BASE64").ok().or(args.seed),
            num_instances: std::env::var("NUM_INSTANCES")
                .ok()
                .and_then(|n| n.parse::<usize>().ok())
                .unwrap_or(args.num_instances),
            nats_stream: std::env::var("NATS_STREAM")
                .map(|url| Url::parse(url.as_ref()).expect("invalid NATS url"))
                .unwrap_or(args.nats_stream),
        }
    }
}
