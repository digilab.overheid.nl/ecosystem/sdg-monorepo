use serde::Serialize;

// Types for Adressen and Gebouwen
#[derive(Debug, Default, Serialize)]
pub struct PlotNumber(pub usize);

#[derive(Clone, Copy, Debug, Serialize)]
pub struct Municipality {
    pub code: &'static str,
    pub name: &'static str,
}

#[derive(Debug, Default, Serialize)]
pub struct Section(pub String);

#[derive(Debug, Default, Serialize)]
pub struct Street(pub &'static str);

#[derive(Debug, Default, Serialize)]
pub struct HouseNumber(pub usize);

#[derive(Debug, Default, Serialize)]
pub struct HouseLetter(pub String); // Note: using String instead of char since char does not have an empty variant (except for \u{0000}, which has a special meaning in some contexts)

#[derive(Debug, Default, Serialize)]
pub struct HouseNumberAddition(pub &'static str);

#[derive(Debug, Default, Serialize)]
pub struct PostalCode(pub String);

#[derive(Debug, Default, Serialize)]
pub struct Purpose(pub &'static str);

#[derive(Debug, Default, Serialize)]
pub struct Surface(pub usize);

#[derive(Debug, Default, Serialize)]
pub struct Registrees(pub usize);

#[derive(Debug, Default, Serialize)]
pub struct Rsin(pub u64);

// Types for Personen
#[derive(Clone, Copy, Debug, Default, Serialize)]
pub struct BSN(pub u32);

#[derive(Debug, Default, Serialize)]
pub struct PlaceOfBirth {
    pub code: &'static str,
    pub name: &'static str, // Note: in the HaalCentraal API: 'omschrijving'
}

#[derive(Debug, Default, Serialize)]
pub struct CountryOfBirth {
    pub code: &'static str,
    pub name: &'static str, // Note: in the HaalCentraal API: 'omschrijving'
}

#[derive(Debug, Default, Serialize)]
pub struct Gender(pub &'static str);

#[derive(Debug, Default, Serialize)]
pub struct FirstName(pub &'static str);

#[derive(Debug, Default, Serialize)]
pub struct Prefix(pub &'static str);

#[derive(Debug, Default, Serialize)]
pub struct FamilyName(pub &'static str);

#[derive(Debug, Default, Serialize)]
pub struct FamilyNameWithPrefix(pub Prefix, pub FamilyName);

// Types for Voertuigen
#[derive(Debug, Default, Serialize)]
pub struct CarModel(pub &'static str);

#[derive(Debug, Default, Serialize)]
pub struct NumberPlate(pub String);
