use crate::{
    bsn_state::BsnState,
    constants::{BIRTH_PLACES_ABROAD, MUNICIPALITIES},
};
use chrono::{DateTime, Duration, NaiveDate, NaiveTime, TimeZone, Utc};
use fake::{faker::chrono::en::DateTimeBetween, Fake, Faker};
use rand::{prelude::SliceRandom, Rng};
use serde::Serialize;
use uuid::Uuid;

use crate::{event::Event, types::*};

#[derive(Debug, Serialize)]
pub struct Birth {
    // The following attributes are stored together in the BRP, see https://stelselvanbasisregistraties.nl/details/METADATA/BRP
    #[serde(rename = "dateOfBirth")]
    pub date_of_birth: NaiveDate,
    #[serde(rename = "placeOfBirth")]
    pub place_of_birth: PlaceOfBirth,
    #[serde(rename = "countryOfBirth")]
    pub country_of_birth: CountryOfBirth, // IMPROVE: change to e.g. "code": "6030", "omschrijving": "Nederland" => get from https://publicaties.rvig.nl/Landelijke_tabellen/Landelijke_tabellen_32_t_m_61_excl_tabel_35/Landelijke_Tabellen_32_t_m_61_in_csv_formaat
}

#[derive(Debug, Serialize)]
pub struct Naming {
    #[serde(rename = "firstName")]
    pub first_name: FirstName,
    #[serde(rename = "prefix")]
    pub prefix: Prefix,
    #[serde(rename = "familyName")]
    pub family_name: FamilyName,
}

#[derive(Debug, Serialize)]
pub struct AddressRegistration {
    pub address: Uuid,
}

#[derive(Debug, Serialize)]
pub struct Person {
    #[serde(skip_serializing)]
    // Note: serializing is currently only used for the birth event, in which case the id is not included for consistency
    pub id: Uuid,
    pub birth: Birth,
    pub bsn: BSN,
    pub gender: Gender,
    pub naming: Naming,
    #[serde(rename = "addressRegistration")]
    pub address_registration: AddressRegistration,
}

impl Person {
    pub fn new<R: Rng + ?Sized>(
        inception: DateTime<Utc>,
        address: Uuid,
        bsn_state: &mut BsnState,
        rng: &mut R,
    ) -> Self {
        // Let all generated people be at most ~80 years old at seed time. IMPROVE: use a population with realistic age distribution instead of using DateTimeBetween
        let day_of_birth: DateTime<Utc> =
            DateTimeBetween(inception - Duration::days(80 * 365), inception).fake_with_rng(rng);
        let place_of_birth: PlaceOfBirth;
        let country_of_birth: CountryOfBirth;

        // Generate a birth place. Note: according to https://www.cbs.nl/nl-nl/visualisaties/dashboard-bevolking/herkomst, on January 1st 2021, ~86% of people living in The Netherlands was born in The Netherlands
        // Generate a random number between 0 and 1
        let random_number: f64 = rng.gen();

        if random_number < 0.86 {
            // Select a random birth place (municipality) in The Netherlands
            let (code, name, _) = MUNICIPALITIES.choose_weighted(rng, |item| item.2).unwrap();
            place_of_birth = PlaceOfBirth { code, name };
            country_of_birth = CountryOfBirth {
                // Source: https://publicaties.rvig.nl/Landelijke_tabellen/Landelijke_tabellen_32_t_m_61_excl_tabel_35/Landelijke_Tabellen_32_t_m_61_in_csv_formaat/Tabel_34_Landen_gesorteerd_op_omschrijving
                code: "6030",
                name: "Nederland",
            };
        } else {
            // Select a random birth place outside The Netherlands
            let (place_name, country_name, country_code, _) = BIRTH_PLACES_ABROAD
                .choose_weighted(rng, |item| item.3)
                .unwrap();
            place_of_birth = PlaceOfBirth {
                code: "",
                name: place_name,
            };
            country_of_birth = CountryOfBirth {
                code: country_code,
                name: country_name,
            };
        }

        let birth = Birth {
            date_of_birth: day_of_birth.date_naive(),
            place_of_birth,
            country_of_birth,
        };
        let bsn = BSN(bsn_state.generate_new(rng));
        let gender = Faker.fake_with_rng::<Gender, R>(rng);
        let family_name_with_prefix = Faker.fake_with_rng::<FamilyNameWithPrefix, R>(rng);
        let naming = Naming {
            first_name: FirstName::new(&gender, rng),
            prefix: family_name_with_prefix.0,
            family_name: family_name_with_prefix.1,
        };
        let address_registration = AddressRegistration { address };

        Self {
            id: Uuid::new_v4(),
            birth,
            bsn,
            gender,
            naming,
            address_registration,
        }
    }

    pub fn to_events<R: Rng>(&self, rng: &mut R) -> Vec<Event> {
        // Set the occurred_at of birth-related events at the same time, since this will normally happen at birth registration
        let day_of_birth_start = Utc.from_utc_datetime(
            &self
                .birth
                .date_of_birth
                .and_time(NaiveTime::from_num_seconds_from_midnight_opt(0, 0).unwrap()),
        );

        // Assume birth registration is done somewhere between 1 and 5 days after birth. IMPROVE: take business days and hours into account
        let birth_registration_time: DateTime<Utc> = DateTimeBetween(
            day_of_birth_start + Duration::days(1),
            day_of_birth_start + Duration::days(5),
        )
        .fake_with_rng(rng);

        let birth_event = self.birth_event(birth_registration_time, rng);
        // let bsn_event = self.bsn_event(birth_registration_time, rng);
        // let gender_event = self.gender_event(birth_registration_time, rng);
        // let name_event = self.name_event(birth_registration_time, rng);
        // let address_registration_event =
        //     self.address_registration_event(birth_registration_time, rng);

        vec![
            // address_registration_event,
            // bsn_event,
            // gender_event,
            // name_event,
            birth_event,
        ]
    }

    pub fn birth_event<R: Rng>(&self, inception: DateTime<Utc>, rng: &mut R) -> Event {
        Event::new(
            self,
            "GeboorteVastgesteld".to_string(),
            Some(inception),
            self.id,
            rng,
        )
    }

    // pub fn bsn_event<R: Rng>(&self, inception: DateTime<Utc>, rng: &mut R) -> Event {
    //     Event::new(
    //         &self.bsn,
    //         "BSNVastgesteld".to_string(),
    //         Some(inception),
    //         self.id,
    //         rng,
    //     )
    // }

    // pub fn gender_event<R: Rng>(&self, inception: DateTime<Utc>, rng: &mut R) -> Event {
    //     Event::new(
    //         &self.gender,
    //         "GeslachtVastgesteld".to_string(),
    //         Some(inception),
    //         self.id,
    //         rng,
    //     )
    // }

    // pub fn name_event<R: Rng>(&self, inception: DateTime<Utc>, rng: &mut R) -> Event {
    //     Event::new(
    //         &self.naming,
    //         "NamenVastgesteld".to_string(),
    //         Some(inception),
    //         self.id,
    //         rng,
    //     )
    // }

    // pub fn address_registration_event<R: Rng>(
    //     &self,
    //     inception: DateTime<Utc>,
    //     rng: &mut R,
    // ) -> Event {
    //     Event::new(
    //         &self.address_registration,
    //         "AdresIngeschreven".to_string(),
    //         Some(DateTimeBetween(inception, inception + Duration::days(16)).fake_with_rng(rng)),
    //         self.id,
    //         rng,
    //     )
    // }
}
