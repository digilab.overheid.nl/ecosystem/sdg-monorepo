mod address;
mod building;
mod person;
mod plot;
mod vehicle;

pub use address::Address;
pub use building::Building;
pub use person::Person;
pub use plot::Plot;
pub use vehicle::Vehicle;
