use fake::{Dummy, Fake, Faker};
use rand::Rng;
use serde::Serialize;
use uuid::Uuid;

use crate::{event::Event, types::*};

#[derive(Debug, Dummy, Serialize)]
pub struct Registration {
    pub model: CarModel,
    #[serde(rename = "numberPlate")]
    pub number_plate: NumberPlate,
}

pub struct Vehicle {
    pub id: Uuid,
    pub registration: Registration,
}

impl Vehicle {
    pub fn new<R: Rng + ?Sized>(rng: &mut R) -> Self {
        Self {
            id: Uuid::new_v4(),
            registration: Faker.fake_with_rng::<Registration, R>(rng),
        }
    }

    pub fn to_events<R: Rng>(&self, rng: &mut R) -> Vec<Event> {
        vec![Event::new(
            &self.registration,
            "VoertuigGeregistreerd".to_string(),
            None,
            self.id,
            rng,
        )]
    }
}
