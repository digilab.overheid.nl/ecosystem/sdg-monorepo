use chrono::{DateTime, Duration, TimeZone, Utc};
use fake::{faker::chrono::en::DateTimeBetween, Fake};
use rand::Rng;
use serde::Serialize;
use serde_json::Value;
use uuid::Uuid;

use crate::serialize;

#[derive(Clone, Debug, Default, Serialize, PartialEq)]
pub struct Event {
    pub id: Uuid,
    #[serde(rename = "occurredAt")]
    pub occurred_at: DateTime<Utc>,
    #[serde(rename = "registeredAt")]
    pub registered_at: DateTime<Utc>,
    #[serde(serialize_with = "serialize::uuid_vec")]
    #[serde(rename = "subjectIds")]
    pub subject_ids: Vec<Uuid>,
    #[serde(rename = "eventType")]
    pub event_type: String,
    #[serde(rename = "eventData")]
    pub event_data: Value,
}

impl Event {
    pub fn new<T: Serialize, R: Rng>(
        mock: T,
        type_: String,
        occurred_at: Option<DateTime<Utc>>,
        subject: Uuid,
        rng: &mut R,
    ) -> Self {
        let registered_at: DateTime<Utc> = Utc.with_ymd_and_hms(1990, 1, 1, 0, 0, 0).unwrap(); // TODO: make configurable, pass via (environment) variable?

        let occurred_at = occurred_at.unwrap_or_else(|| {
            DateTimeBetween(
                registered_at - Duration::days(100 * 365), // ~100 years before registration
                registered_at,
            )
            .fake_with_rng(rng)
        });

        Event {
            id: Uuid::new_v4(),
            occurred_at,
            registered_at,
            subject_ids: vec![subject],
            event_type: type_.to_string(),
            event_data: serde_json::to_value(mock).expect("invalid mock"),
        }
    }
}
