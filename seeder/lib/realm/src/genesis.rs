use base64::{engine::general_purpose::STANDARD as b64, Engine};
use rand::{rngs::StdRng, Rng, SeedableRng};

use crate::config::Config;

#[derive(PartialEq, Debug)]
pub enum Stage {
    Addresses,
    Humans,
    Plots,
    Buildings,
    Vehicles,
}

pub struct Genesis {
    pub max_instances: usize,
    pub rng: StdRng,
}

impl Genesis {
    pub fn new(config: Config) -> Self {
        let rng = StdRng::from_seed(if let Some(seed) = config.seed {
            b64.decode(seed)
                .expect("invalid base64")
                .try_into()
                .expect("invalid seed length")
        } else {
            rand::thread_rng().gen::<[u8; 32]>()
        });

        Self {
            max_instances: config.num_instances,
            rng,
        }
    }
}
