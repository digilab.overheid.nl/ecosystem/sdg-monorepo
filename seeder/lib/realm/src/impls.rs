use chrono::{DateTime, Utc};
use rand::{prelude::SliceRandom, rngs::StdRng};
use uuid::Uuid;

use synthetic_data::{Address, BsnState, Building, Event, Person, Plot, Vehicle};

use crate::genesis::{Genesis, Stage};

pub struct GenesisIterator {
    current_instance: Vec<Event>,
    max_instances: usize,
    num_instances: usize,
    addresses: Vec<(Uuid, DateTime<Utc>)>,
    persons: Vec<Person>,
    rng: StdRng,
    stage: Stage,
    bsn_state: BsnState,
}

impl GenesisIterator {
    #[inline(always)]
    fn stage_complete(&self) -> bool {
        self.num_instances == self.max_instances
    }

    fn step_forward(&mut self) {
        self.stage = match self.stage {
            Stage::Addresses => Stage::Humans,
            Stage::Humans => Stage::Plots,
            Stage::Plots => Stage::Buildings,
            Stage::Buildings => Stage::Vehicles,
            _ => unimplemented!(),
        };
        self.num_instances = 0;
    }

    fn instantiate_object(&mut self) {
        let rng = &mut self.rng;
        self.current_instance = match self.stage {
            Stage::Plots => Plot::new(rng).to_events(rng),
            Stage::Addresses => {
                let address = Address::new(rng);
                let events = address.to_events(rng);
                self.addresses.push((address.id, events[0].registered_at));
                events
            }
            Stage::Vehicles => Vehicle::new(rng).to_events(rng),
            Stage::Humans => {
                let (address, inception) = self.addresses.choose(rng).expect("cache miss");
                let person = Person::new(*inception, *address, &mut self.bsn_state, rng);
                let events = person.to_events(rng);
                self.persons.push(person);
                events
            }
            Stage::Buildings => {
                let (address, inception) = self.addresses.choose(rng).expect("cache miss");
                let owner = self.persons.choose(rng).expect("cache miss");
                Building::new(*inception, *address, owner, &self.persons, rng).to_events(rng)
            }
        };
        self.num_instances += 1;

        if self.stage_complete() && self.stage != Stage::Vehicles {
            self.step_forward();
        }
    }
}

impl IntoIterator for Genesis {
    type Item = Event;
    type IntoIter = GenesisIterator;

    fn into_iter(self) -> GenesisIterator {
        GenesisIterator {
            current_instance: vec![],
            max_instances: self.max_instances,
            num_instances: 0,
            addresses: vec![],
            persons: vec![],
            rng: self.rng,
            stage: Stage::Addresses,
            bsn_state: BsnState::default(),
        }
    }
}

impl Iterator for GenesisIterator {
    type Item = Event;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current_instance.is_empty() && !self.stage_complete() {
            self.instantiate_object();
        }

        self.current_instance.pop()
    }
}
