package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"text/template"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/html/v2"
	"github.com/sqids/sqids-go"

	api "gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-frontend/helpers"
)

var (
	apiClient     *api.ClientWithResponses
	sqidsInstance *sqids.Sqids
)

// Constants for pagination
const (
	rowsPerPage int = 10
	onEachSide  int = 2
	onEnds      int = 2
)

func init() {
	// Get the FRP backend endpoint from the environment variables
	backendEndpoint := os.Getenv("APP_FRP_BACKEND_ENDPOINT")
	if backendEndpoint == "" {
		backendEndpoint = "https://brp-api-mock.apps.digilab.network/haalcentraal/api/brp" // Fallback value
	}

	// Create the API client
	var err error
	if apiClient, err = api.NewClientWithResponses(backendEndpoint); err != nil {
		log.Fatalf("error creating API client: %v", err)
	}

	// Encode and decode BSNs using Sqids. Note: used as poor man's encryption, in order not to show BSNs directly in the URLs. Note: this does not improve security
	if sqidsInstance, err = sqids.New(sqids.Options{
		MinLength: 10,
		// IMPROVE: use a custom alphabet
	}); err != nil {
		log.Fatalf("error creating Squids instance: %v", err)
	}
}

type pagination struct {
	Pages      []int // Note: 0 means: ellipsis (...)
	PageNumber int
	NumPages   int
}

// rng returns a range of integers from `from` to `to`, inclusive. See also the note below about the Python range() function
func rng(from, to int) []int {
	pages := make([]int, 0, to-from+1) // Note: we assume that to >= from. The code below could be optimized a bit, but is written like this for clarity reasons
	for i := from; i <= to; i++ {
		pages = append(pages, i)
	}

	return pages
}

func main() {
	// Parse the optional port flag
	port := flag.Int("port", 80, "port on which to run the web server")
	flag.Parse()

	// Template engine and functions
	engine := html.New("./views", ".html")

	engine.AddFuncMap(template.FuncMap{
		"encodeBSN": func(bsn string) (result string, err error) {
			// Parse the BSN as uint64
			var parsed uint64
			parsed, err = strconv.ParseUint(bsn, 10, 64)
			if err != nil {
				err = fmt.Errorf("error parsing BSN as uint64: %w", err)
				return
			}

			if result, err = sqidsInstance.Encode([]uint64{parsed}); err != nil {
				err = fmt.Errorf("error encoding BSN: %w", err)
			}

			return
		},
		"isEven":     func(i int) bool { return i%2 == 0 },
		"formatTime": helpers.FormatTime,
		"formatDate": helpers.FormatDate,
		"add": func(i, j int) int {
			return i + j
		},
		"sub": func(i, j int) int {
			return i - j
		},
		// getPagination returns 'elided' pagination for the specified number of rows and page query string parameter, similar to what e.g. Django has, see https://docs.djangoproject.com/en/dev/ref/paginator/#django.core.paginator.Paginator.get_elided_page_range
		"getPagination": func(metadata api.Metadata) (res pagination) {
			res.NumPages = max((metadata.TotalCount+rowsPerPage-1)/rowsPerPage, 1) // Note: similar to math.Ceil
			res.PageNumber = min(metadata.Offset+1, res.NumPages)                  // Note: see comment about Offset below

			// Note: the logic below is similar to that used in the Django code, see https://github.com/django/django/blob/main/django/core/paginator.py, except that the rng function is including the second argument, unlike the python range() function

			// In case of a short range, return it
			if res.NumPages <= (onEachSide+onEnds)*2 {
				res.Pages = rng(1, res.NumPages)

				return
			}

			if res.PageNumber > onEachSide+onEnds+2 {
				res.Pages = append(append(
					rng(1, onEnds),
					0), // Ellipsis
					rng(res.PageNumber-onEachSide, res.PageNumber)...)
			} else {
				res.Pages = rng(1, res.PageNumber)
			}

			if res.PageNumber < res.NumPages-onEachSide-onEnds-1 {
				res.Pages = append(append(
					append(res.Pages, rng(res.PageNumber+1, res.PageNumber+onEachSide)...),
					0), // Ellipsis
					rng(res.NumPages-onEnds+1, res.NumPages)...)
			} else {
				res.Pages = append(res.Pages, rng(res.PageNumber+1, res.NumPages)...)
			}

			return
		},
	})

	// Fiber instance
	app := fiber.New(fiber.Config{
		Views:       engine,
		ViewsLayout: "layouts/main",
	})

	// Middleware
	app.Use(logger.New(logger.Config{
		Format: "${time} | ${status} | ${latency} | ${method} | ${path}   ${error}\n", // Do not log IP addresses. Note: see https://docs.gofiber.io/api/middleware/logger#constants for more logger variables
	}))

	// Routes
	app.Static("/", "./public")

	// Healthz endpoint
	app.Get("/healthz", func(c *fiber.Ctx) error {
		return c.SendString(".") // Send a period as response body, similar to chi's Heartbeat middleware
	})

	// security.txt redirect
	app.Get("/.well-known/security.txt", func(c *fiber.Ctx) error {
		return c.Redirect("https://www.ncsc.nl/.well-known/security.txt", fiber.StatusFound) // StatusFound is HTTP code 302
	})

	app.Get("/", func(c *fiber.Ctx) error {
		// Fetch the people from the backend. Note: we use the GetPersonen endpoint, which returns all people in the db, and is not supported by the HaalCentraal API
		ctx := context.Background()

		var offset int
		if pageNumber := c.QueryInt("page", 1); pageNumber > 1 {
			offset = pageNumber - 1 // Note: Offset in the API client is expected to be 1, 2, ... and should not be multiplied by rowsPerPage
		}
		limit := rowsPerPage

		resp, err := apiClient.GetPersonenWithResponse(ctx, api.GetPersonen{
			Fields: []string{"burgerservicenummer", "geboorte.datum", "naam", "gemeenteVanInschrijving.omschrijving"},
			Limit:  &limit,
			Offset: &offset,
		})
		if err != nil {
			return err
		}

		if resp.StatusCode() != 200 {
			// If the response contains 'unknown claim type', return a special error
			if strings.Contains(string(resp.Body), "unknown claim type") {
				return c.Render("index", fiber.Map{"Data": nil})
			}

			// Return the repsonse
			return errors.New(string(resp.Body)) // IMPROVE: better formatting
		}

		return c.Render("index", fiber.Map{"Data": resp.JSON200})
	})

	people := app.Group("/people")

	// Route to view person details
	people.Get("/:id", func(c *fiber.Ctx) error {
		// Decode the ID (sqids). Note: used as poor man's encryption for the BSNs, in order not to show them directly in the URLs, although this does not improve security
		var bsn string
		if decoded := sqidsInstance.Decode(c.Params("id")); len(decoded) == 0 {
			return fmt.Errorf("cannot decode ID: %s", c.Params("id"))
		} else {
			bsn = strconv.FormatUint(decoded[0], 10)
		}

		// Fetch the person from the backend
		var query api.PersonenQuery
		query.FromRaadpleegMetBurgerservicenummer(api.RaadpleegMetBurgerservicenummer{
			Burgerservicenummer: []string{bsn},
			Fields:              []string{"burgerservicenummer", "geboorte", "naam", "geslacht", "gemeenteVanInschrijving", "adressering", "overlijden", "partners", "kinderen"},
		})

		ctx := context.Background()

		resp, err := apiClient.PersonenWithResponse(ctx, query)
		if err != nil {
			return err
		}

		if resp.StatusCode() != 200 {
			// Return the repsonse
			return errors.New(string(resp.Body)) // IMPROVE: better formatting
		}

		personResp, err := resp.JSON200.AsRaadpleegMetBurgerservicenummerResponse()
		if err != nil {
			return err
		}

		if personResp.Personen == nil || len(*personResp.Personen) == 0 {
			return errors.New("geen personen in response")
		}

		persoon := (*personResp.Personen)[0]

		return c.Render("person-details", fiber.Map{"person": persoon})
	})

	// Start server
	if err := app.Listen(fmt.Sprintf(":%d", *port)); err != nil { // Note: port should never be nil, since flag.Parse() is run
		log.Println("error starting server:", err)
	}
}
