FROM digilabpublic.azurecr.io/golang:1.22.2-alpine3.19

RUN ["go", "install", "github.com/githubnemo/CompileDaemon@latest"]

# Install Node.js. Note: the nodejs package does not include corepack, so we use nodejs-current
RUN apk add --no-cache nodejs-current

RUN corepack enable && corepack prepare pnpm@latest --activate

# Cache dependencies
WORKDIR /build/frp-api
COPY frp-api/go.mod frp-api/go.sum ./

WORKDIR /build/frp-frontend
COPY frp-frontend/go.mod frp-frontend/go.sum ./

RUN go mod download

WORKDIR /build/frp-api
COPY frp-api ./

WORKDIR /build/frp-frontend
COPY frp-frontend/package.json frp-frontend/pnpm-lock.yaml frp-frontend/tailwind.config.js frp-frontend/.postcssrc frp-frontend/.parcelrc ./

RUN pnpm install

COPY frp-frontend/main.go .
COPY frp-frontend/public public
COPY frp-frontend/static static
COPY frp-frontend/views views
COPY frp-frontend/helpers helpers

ENTRYPOINT sh -c 'pnpm run dev & CompileDaemon -log-prefix=false -pattern="(.+\.go|.+\.html|.+\.js|.+\.css|.+\.sql)$" -exclude-dir=.git -exclude-dir=node_modules -exclude-dir=.parcel-cache -build="go build -o server ." -command="./server serve"'
