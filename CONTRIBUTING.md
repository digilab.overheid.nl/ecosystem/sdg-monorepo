# Contributing

## Setup your development environment

### Requirements

We use [asdf](https://asdf-vm.com/) for installing [required tools](.tool-versions):
```sh
asdf plugin add golang
asdf plugin add golangci-lint
asdf plugin add jq
asdf plugin add k3d
asdf plugin add kubectl
asdf plugin add kustomize
asdf plugin add pre-commit
asdf plugin add skaffold
asdf install
```


## Running locally

Running locally uses k3d, so we need a working docker environment.

We use pre-commmit for providing a git hook:
```sh
pre-commit install
```

The first time you run the project, getting k3d to work takes a while. Wait for all pods to be running:
```sh
make k3d
kubectl get pods -Aw
```

After that, you can just do:
```sh
make dev
```

To add test fixtures:
```sh
make seed
```

Open [index-127.0.0.1.nip.io:8080](http://index-127.0.0.1.nip.io:8080) for the landing page, linking to all services.

### Testing

```sh
make test
```

### Example URLs

TODO: add some example URLs

These are some URLs that should work (but really you should use some openapi helper tool like SwaggerUI to generate requests for you):
- Create a new world:
  ```
  curl -X POST \
    --json @- \
    'http://otherdevorg-world-backend-127.0.0.1.nip.io:8080/v0/worlds' \
    << EOT
  {
    "data": {
      "name": "Myplanet"
    }
  }
  EOT
  ```
- Find a world by id:
  ```
  curl -H 'Accept: application/json' \
    'http://otherdevorg-world-backend-127.0.0.1.nip.io:8080/v0/worlds/c9fd1c78-06e1-4bdb-9c03-8793bcea268a'
  ```

For more URLs, look in `test/integration`.


## External documentation

TODO: Add useful domain-specific documentation links
