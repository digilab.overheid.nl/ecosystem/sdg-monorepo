use bevy::prelude::*;
use std::collections::HashMap;
use uuid::Uuid;

// AddressState contains a map from address IDs to bevy entities
#[derive(Resource, Default)]
pub struct AddressState {
    data: HashMap<Uuid, Entity>,
}

impl AddressState {
    pub fn push(&mut self, id: Uuid, index: Entity) {
        self.data.insert(id, index);
    }

    pub fn get(&self, id: &Uuid) -> Option<Entity> {
        self.data.get(id).map(|v| v.to_owned())
    }
}
