use bevy::prelude::*;
use bevy_turborand::{DelegatedRng, RngComponent};
use std::collections::HashSet;

// Note: BSNs (citizen service numbers) should be generated randomly, according to the specification on https://www.rvig.nl/node/1351
// Since a BSN may not be re-used, we keep track of all used BSNs
// There are exactly 90 million possible BSNs
// It might be theoretically better to have a function that maps all numbers from 1 to 90 million to a pseudo-random BSN and keep only track of the counter. However, there is no guarantee that the seeded BSNs are sequential
// Note that a BSN can either have 8 or 9 digits, since according to the [2016 spec](https://web.archive.org/web/20181003141457/https://www.rvig.nl/binaries/rvig/documenten/richtlijnen/2016/09/09/logisch-ontwerp-bsn-versie-1.4/Logisch+Ontwerp+BSN+1.4.pdf), 8 digits are possible. According to the [2024 spec](https://www.rvig.nl/sites/default/files/2023-12/Logisch%20Ontwerp%20BSN%202024.Q1.pdf) and https://www.rvig.nl/node/1351, currently 9 digits are required

#[derive(Resource, Default)]
pub struct BsnState {
    used: HashSet<u32>,
}

// is_valid_bsn returns whether or not the specified integer is a valid BSN
// Note: for slight performance benefits, the function assumes that the specified candidate already has the correct number of digits (8 or 9)
fn is_valid_bsn(candidate: u32) -> bool {
    let mut num = candidate;
    let mut sum: i32 = 0;

    // Loop over all digits, beginning with the rightmost digit. Note: this approach seems faster than converting the number to string, extracting the digits and converting them to integers again
    let mut i: i32 = 1;
    while num != 0 {
        let digit = (num % 10) as i32;

        // Add the digits to the sum, multiplying by 1..9
        if i == 1 {
            sum = -digit; // IMPROVE: store this value and subtract it later, so we can use u32 instead of i32? Then first check if the cadidate is >= 10 (or actually, >= 10000000)
        } else {
            sum += i * digit;
        }

        num /= 10; // Note: the result will be floored/truncated, so no need to subtract `digit` from `num` first
        i += 1;
    }

    sum % 11 == 0
}

impl BsnState {
    pub fn generate_new(&mut self, rng: &mut RngComponent) -> u32 {
        // Generate random candidates until a valid unused candidate is found
        let mut candidate = rng.u32(10000008..=999999990); // Note: 10000008 is the lowest valid 8-digit BSN and 999999990 is the highest 9-digit BSN
        while !is_valid_bsn(candidate) || !self.used.insert(candidate) {
            candidate = rng.u32(10000008..=999999990);
        }
        candidate
    }

    pub fn mark_as_used(&mut self, bsn: u32) {
        self.used.insert(bsn);
    }
}
