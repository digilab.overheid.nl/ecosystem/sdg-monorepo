use bevy_turborand::{DelegatedRng, RngComponent};
use chrono::{NaiveDate, NaiveDateTime};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use uuid::{Builder as UuidBuilder, Uuid};
use crate::components::Age;

pub use sdg_event_simulator_common::{config::SdgConfig, event::Event};

use crate::components::GenderValue;

pub const BIRTH_TOPIC: &str = "events.frp.persoon.GeboorteVastgesteld";
pub const NAME_TOPIC: &str = "events.frp.persoon.NamenGewijzigd";
pub const GENDER_TOPIC: &str = "events.frp.persoon.GeslachtGewijzigd";
pub const DEATH_TOPIC: &str = "events.frp.persoon.OverlijdenVastgesteld";
pub const ADDRESS_TOPIC: &str = "events.frag.adres.Geregistreerd"; // The topic on which addresses are sent by the seeder and FRAG (Fictief Register Adressen en Gebouwen) simulator

// pub const PESON_ADDRESS_TOPIC: &str = "events.frp.persoon.AdresIngeschreven"; // The topic on which to post when a person moves to another address

#[derive(Debug, Serialize, Deserialize)]
pub struct Birth {
    // The following attributes are stored together in the BRP, see https://stelselvanbasisregistraties.nl/details/METADATA/BRP
    #[serde(rename = "dateOfBirth")]
    pub date_of_birth: NaiveDate,
    #[serde(rename = "placeOfBirth")]
    pub place_of_birth: PlaceOfBirth,
    #[serde(rename = "countryOfBirth")]
    pub country_of_birth: CountryOfBirth, // IMPROVE: change to e.g. "code": "6030", "omschrijving": "Nederland" => get from https://publicaties.rvig.nl/Landelijke_tabellen/Landelijke_tabellen_32_t_m_61_excl_tabel_35/Landelijke_Tabellen_32_t_m_61_in_csv_formaat
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct PlaceOfBirth {
    pub code: String,
    pub name: String, // Note: in the HaalCentraal API: 'omschrijving'
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct CountryOfBirth {
    pub code: String,
    pub name: String, // Note: in the HaalCentraal API: 'omschrijving'
}

#[derive(Serialize, Deserialize, Debug)]
pub struct EventBirthData {
    pub birth: Birth,
    pub bsn: u32,
    pub gender: String,
    pub naming: Naming,
    #[serde(rename = "addressRegistration")]
    pub address_registration: AddressRegistration,
}

// The following attributes are stored together in the BRP, see https://stelselvanbasisregistraties.nl/details/METADATA/BRP
#[derive(Serialize, Deserialize, Debug)]
pub struct Naming {
    #[serde(rename = "firstName")]
    pub first_name: String,
    #[serde(rename = "prefix")]
    pub prefix: String,
    #[serde(rename = "familyName")]
    pub family_name: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct AddressRegistration {
    pub address: Uuid,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EventBsnData(pub u32);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EventGenderData(pub String);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EventNameData {
    #[serde(rename = "firstName")]
    pub first_name: String,
    #[serde(rename = "prefix")]
    pub prefix: String,
    #[serde(rename = "familyName")]
    pub family_name: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Municipality {
    pub code: String,
    pub name: String,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Purpose(pub String);

// pub struct EventPersonAddressRegistationData {
//     pub address: String,
//     pub municipality: Municipality,
//     pub purpose: Purpose,
// }

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EventAddressRegistationData {
    pub municipality: Municipality,
    pub purpose: Purpose,
}

// birth_registration_event returns a birth registration event including BSN, etc. Note: no event is emitted on birth itself
pub fn birth_registration_event(
    rng: &mut RngComponent,
    now: &NaiveDateTime,
    subject_id: Uuid,
    bsn: u32,
    gender: &GenderValue,
    age: &Age,
    first_name: String,
    prefix: String,
    family_name: String,
    address_id: Uuid,
    place_of_birth: PlaceOfBirth,
    country_of_birth: CountryOfBirth,
) -> Value {
    // Generate an event UUID
    let mut bytes = [0; 16];
    rng.fill_bytes(&mut bytes);
    let stable_uuid = UuidBuilder::from_bytes(bytes).into_uuid();

    let data = EventBirthData {
        birth: Birth {
            date_of_birth: age.birth_date.into(),
            place_of_birth: place_of_birth,
            country_of_birth: country_of_birth,
        },
        bsn,
        gender: gender.to_string(),
        naming: Naming {
            first_name,
            prefix,
            family_name,
        },
        address_registration: AddressRegistration {
            address: address_id,
        },
    };

    let event = Event {
        id: stable_uuid,
        occured_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        registered_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        subject_ids: vec![subject_id],
        event_type: "GeboorteVastgesteld".into(),
        event_data: serde_json::to_value(data).unwrap(),
    };

    serde_json::to_value(event).unwrap()
}

pub fn name_change_event(
    rng: &mut RngComponent,
    now: &NaiveDateTime,
    subject_id: Uuid,
    first_name: &str,
    prefix: &str,
    family_name: &str,
) -> Value {
    let mut bytes = [0; 16];
    rng.fill_bytes(&mut bytes);
    let stable_uuid = UuidBuilder::from_bytes(bytes).into_uuid();

    let data = EventNameData {
        first_name: first_name.into(),
        prefix: prefix.into(),
        family_name: family_name.into(),
    };

    let event = Event {
        id: stable_uuid,
        occured_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        registered_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        subject_ids: vec![subject_id],
        event_type: "NamenGewijzigd".into(),
        event_data: serde_json::to_value(data).unwrap(),
    };

    serde_json::to_value(event).unwrap()
}

pub fn gender_change_event(
    rng: &mut RngComponent,
    now: &NaiveDateTime,
    subject_id: Uuid,
    gender: &GenderValue,
) -> Value {
    let mut bytes = [0; 16];
    rng.fill_bytes(&mut bytes);
    let stable_uuid = UuidBuilder::from_bytes(bytes).into_uuid();

    let data = EventGenderData(gender.to_string());

    let event = Event {
        id: stable_uuid,
        occured_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        registered_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        subject_ids: vec![subject_id],
        event_type: "GeslachtGewijzigd".into(),
        event_data: serde_json::to_value(data).unwrap(),
    };

    serde_json::to_value(event).unwrap()
}

pub fn death_event(rng: &mut RngComponent, now: &NaiveDateTime, subject_id: Uuid) -> Value {
    let mut bytes = [0; 16];
    rng.fill_bytes(&mut bytes);
    let stable_uuid = UuidBuilder::from_bytes(bytes).into_uuid();

    let event = Event {
        id: stable_uuid,
        occured_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        registered_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        subject_ids: vec![subject_id],
        event_type: "OverlijdenVastgesteld".into(),
        event_data: json!({}),
    };

    serde_json::to_value(event).unwrap()
}

// pub fn address_registration_event(
//     rng: &mut RngComponent,
//     now: &NaiveDateTime,
//     subject_id: Uuid,
//     address: Uuid,
// ) -> Value {
//     let mut bytes = [0; 16];
//     rng.fill_bytes(&mut bytes);
//     let stable_uuid = UuidBuilder::from_bytes(bytes).into_uuid();

//     let data = EventAddressRegistationData {
//         address: address.to_string(),
//     };

//     let event = Event {
//         id: stable_uuid,
//         occured_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
//         registered_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
//         subject_ids: vec![subject_id],
//         event_type: "AdresIngeschreven".into(),
//         event_data: serde_json::to_value(data).unwrap(),
//     };

//     serde_json::to_value(event).unwrap()
// }
