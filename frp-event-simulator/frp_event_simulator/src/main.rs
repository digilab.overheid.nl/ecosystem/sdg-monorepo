use std::thread;
use std::{env, process::exit};

use bevy::app::{AppLabel, SubApp};
use bevy::prelude::*;
use nats::jetstream::{BatchOptions, ConsumerConfig, PullSubscribeOptions, PullSubscription};
use nats::Message;
use sdg_event_simulator_common::bevy::resources::clock_state::ClockState;

use sdg_event_simulator_common::{
    bevy::{
        plugins::SdgPluginGroup,
        resources::{nats_state::NatsState, simulator_identity::SimulatorIdentity},
    },
    flags::{setup_signals, wait_for_exit},
};

mod components;
mod data;
mod event;
mod resources;
mod systems;
mod errors;
mod logger;

use resources::{AddressState, BsnState, SeedState};
use systems::{init_systems, update_systems};
use errors::AppError;

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, AppLabel)]
struct OrchestratorApp;

fn main() -> Result<(), AppError> {
    logger::init()?;

    info!("Starting simulator");

    setup_signals();

    let mut app = App::new();
    app.set_runner(run_loop_master)
        .add_plugins(SdgPluginGroup)
        .insert_resource(SimulatorIdentity::new("frp-event-simulator"));

    app.insert_sub_app(OrchestratorApp, SubApp::new(get_new_sub_world(), |_, _| {}));

    app.run();

    let sleep_on_end = env::var("SLEEP").map(|v| v != "n").unwrap_or_else(|_| true);
    if sleep_on_end {
        wait_for_exit();
    }

    exit(0)
}

fn get_new_sub_world() -> App {
    let mut sub_app = App::new();
    sub_app
        .add_plugins(SdgPluginGroup)
        .insert_resource(SimulatorIdentity::new("frp-event-simulator"))
        .insert_resource(SeedState::default())
        .insert_resource(BsnState::default())
        .insert_resource(AddressState::default())
        .add_systems(PreStartup, init_systems::load_seed_births)
        .add_systems(Startup, init_systems::load_seed_addresses)
        .add_systems(PostStartup, init_systems::load_seed_deaths)
        .add_systems(PostStartup, init_systems::inspect_seeded)
        .add_systems(Update, update_systems::sim_pregnancy)
        .add_systems(Update, update_systems::sim_birth)
        .add_systems(Update, update_systems::sim_birth_registration)
        .add_systems(Update, update_systems::sim_name_change)
        .add_systems(Update, update_systems::sim_gender_change)
        // .add_systems(Update, update_systems::sim_address_registration)
        .add_systems(Last, update_systems::sim_death)
        .add_systems(Update, update_systems::sim_data_error_correction);


    debug!("sub application created");

    return sub_app;
}

pub fn run_loop_master(mut app: App) {
    let sim_ident = app.world.get_resource::<SimulatorIdentity>().unwrap();
    let nats_connection = app.world.get_resource::<NatsState>().unwrap();
    let command_stream = get_stream(&nats_connection.uri, sim_ident.get_name(), "command");
    let time_stream = get_stream(&nats_connection.uri, sim_ident.get_name(), "time");

    loop {
        let command = get_command(&command_stream);

        match command {
            CommandRes::Some(command, msg) => {
                match command.as_str() {
                    "reset" => {
                        // Delete the current sub app running the sim
                        let res = app.remove_sub_app(OrchestratorApp);
                        match res {
                            Some(_) => info!("sub app deleted, waitin for restart"),
                            None => warn!("app not found"),
                        }
                    },
                    "start" => {
                        app.insert_sub_app(OrchestratorApp, SubApp::new(get_new_sub_world(), |_, _| {}));
                    }
                    _ => {}
                }

                msg.ack().unwrap();
            }
            CommandRes::Error(e) => panic!("Failed to get command: {}", e),
            _ => {},
        }

        let time = get_time(&time_stream);

        match time {
            TimeRes::Some(time, msg) => {
                match app.get_sub_app_mut(OrchestratorApp) {
                    Ok(sub_app) => {
                        let mut counter_state = sub_app.world.get_resource_mut::<ClockState>().unwrap();

                        counter_state.set_time(&time);

                        app.update();

                        msg.ack().unwrap();
                    }
                    Err(err) => {
                        warn!("Failed to get sub app, should be alive by now: {}", err.as_str());

                        msg.ack().unwrap();
                    }
                }
            }
            TimeRes::None => thread::sleep(std::time::Duration::from_millis(100)),
            TimeRes::Error(e) => panic!("Failed to get time: {}", e),
        }

    }



}

fn get_stream(nats_uri: &str, identity: &str, subject: &str) -> PullSubscription {
    let tnc = nats::connect(nats_uri).expect("Could not connect to nats");

    let options = nats::JetStreamOptions::new();
    let stream = nats::jetstream::JetStream::new(tnc, options);

    info!("trying to connect to: {}", subject);

    return stream
        .pull_subscribe_with_options(
            subject,
            &PullSubscribeOptions::new().consumer_config(ConsumerConfig {
                durable_name: Some(identity.to_string()),
                deliver_policy: nats::jetstream::DeliverPolicy::All,
                replay_policy: nats::jetstream::ReplayPolicy::Instant,
                ..Default::default()
            }),
        )
        .unwrap();
}

enum TimeRes {
    Some(String, Message),
    None,
    Error(std::io::Error),
}

fn get_time(s: &PullSubscription) -> TimeRes {
    let time = s.fetch(BatchOptions {
        batch: 1,
        expires: None,
        no_wait: true,
    });

    match time {
        Ok(mut t) => {
            let message = t.next();
            if let Some(message) = message {
                let res = std::str::from_utf8(&message.data).unwrap().to_string();
                TimeRes::Some(res, message)
            } else {
                TimeRes::None
            }
        }
        Err(e) => {
            match e.kind() {
                std::io::ErrorKind::TimedOut => warn!("Time timed out"),
                std::io::ErrorKind::Other => warn!("Time disconnected"),
                _ => todo!(),
            };
            TimeRes::Error(e)
        }
    }
}

enum CommandRes {
    Some(String, Message),
    None,
    Error(std::io::Error),
}

fn get_command(s: &PullSubscription) -> CommandRes {
    let f = s.fetch(BatchOptions {
        batch: 1,
        expires: None,
        no_wait: true,
    });

    match f {
        Ok(mut t) => {
            let message = t.next();
            if let Some(message) = message {
                let res = std::str::from_utf8(&message.data).unwrap().to_string();

                match res.as_str() {
                    "reset" | "start" => {
                        debug!("received command: {}", message);
                        CommandRes::Some(res, message)
                    },
                    _ => {
                        warn!("message data unknown: {}", res);
                        CommandRes::None
                    },
                }
            } else {
                CommandRes::None
            }
        }
        Err(err) => {
            match err.kind() {
                std::io::ErrorKind::TimedOut => warn!("Time timed out"),
                std::io::ErrorKind::Other => warn!("Time disconnected"),
                _ => todo!(),
            };
            CommandRes::Error(err)
        }
    }
}
