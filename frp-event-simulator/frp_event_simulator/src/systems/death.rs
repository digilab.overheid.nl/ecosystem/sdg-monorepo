use bevy::prelude::*;
use bevy_turborand::RngComponent;
use sdg_event_simulator_common::bevy::resources::{clock_state::ClockState, nats_state::NatsState};

use crate::{
    components::*,
    event::{death_event, DEATH_TOPIC},
};

#[allow(clippy::type_complexity)]
pub fn sim_death(
    mut commands: Commands,
    mut query: Query<(Entity, &mut RngComponent, &Age, &Id), With<Person>>,
    clock_state: Res<ClockState>,
    nats_state: Res<NatsState>,
) {
    for (entity, mut rng, age, id) in query.iter_mut() {
        let now = clock_state.get_time();
        if age.event_death_occurs(&mut rng, &clock_state) {
            // Output some information
            info!(
                "person died, birth date: {}, age: {} years",
                age.birth_date,
                age.age(now).num_days() / 365
            );

            let message = death_event(&mut rng, &now, id.uuid);

            nats_state
                .connection
                .publish(DEATH_TOPIC, message.to_string())
                .unwrap();

            commands.entity(entity).despawn();

            debug!(time = now.to_string(), eid = entity.index(), "DEATH   ");
        }
    }
}
