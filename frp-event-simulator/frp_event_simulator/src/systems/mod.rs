mod address;
mod birth;
mod data_error;
mod death;
mod gender;
mod init;
mod name;

pub mod init_systems {
    pub use super::init::inspect_seeded;
    pub use super::init::load_seed_addresses;
    pub use super::init::load_seed_births;
    pub use super::init::load_seed_deaths;
}

pub mod update_systems {
    // pub use super::address::sim_address_registration;
    pub use super::birth::sim_birth;
    pub use super::birth::sim_birth_registration;
    pub use super::birth::sim_pregnancy;
    pub use super::data_error::sim_data_error_correction;
    pub use super::death::sim_death;
    pub use super::gender::sim_gender_change;
    pub use super::name::sim_name_change;
}
