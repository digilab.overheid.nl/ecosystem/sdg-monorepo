use bevy::prelude::*;
use bevy_turborand::prelude::*;
use chrono::NaiveDateTime;
use nats::jetstream::{
    self, BatchOptions, ConsumerConfig, JetStream, JetStreamOptions, PullSubscribeOptions,
    PullSubscription,
};
use sdg_event_simulator_common::bevy::resources::{clock_state::ClockState, nats_state::NatsState};
use tracing::warn;

use crate::{
    components::{
        Address, AddressReference, Age, CanBePregnant, Gender, GenderValue, Id, Person,
        PersonBundle, PersonName, BSN,
    },
    event::{
        Event, EventAddressRegistationData, EventBirthData, ADDRESS_TOPIC, BIRTH_TOPIC, DEATH_TOPIC,
    },
    resources::{AddressState, BsnState, SeedState},
};

#[derive(Component)]
pub struct FromSeed;

pub fn get_seed_stream(nats_uri: &str, topic: &str) -> PullSubscription {
    let connection = nats::connect(nats_uri).expect("Could not connect to nats");
    let options = JetStreamOptions::new();
    let stream = JetStream::new(connection, options);

    stream
        .pull_subscribe_with_options(
            topic,
            &PullSubscribeOptions::new().consumer_config(ConsumerConfig {
                // durable_name: Some("sdg_person".to_string()),
                deliver_policy: jetstream::DeliverPolicy::All,
                replay_policy: jetstream::ReplayPolicy::Instant,
                filter_subject: topic.to_string(),
                ..Default::default()
            }),
        )
        .unwrap()
}

pub fn load_seed_births(
    mut commands: Commands,
    mut global_rng: ResMut<GlobalRng>,
    nats_state: Res<NatsState>,
    mut seed_state: ResMut<SeedState>,
    mut bsn_state: ResMut<BsnState>,
) {
    info!("Load `seed data birth` start");

    let stream = get_seed_stream(&nats_state.uri, BIRTH_TOPIC);

    loop {
        let messages = stream.fetch(BatchOptions {
            batch: 100,
            expires: None,
            no_wait: true,
        });

        let messages = match messages {
            Ok(m) => m,
            Err(e) => {
                error!("Error loading events batch {}", e);
                break;
            }
        };

        let mut message_count = 0;
        for message in messages {
            // Parse data
            let data = std::str::from_utf8(&message.data).unwrap();
            let event: Event = serde_json::from_str(data).unwrap();
            let birth_date =
                NaiveDateTime::parse_from_str(&event.occured_at, "%Y-%m-%dT%H:%M:%S%.fZ").unwrap();
            let subject = event.subject_ids[0];
            let event_data: EventBirthData = serde_json::from_value(event.event_data).unwrap();
            let bsn = BSN(event_data.bsn);
            let gender = Gender(GenderValue::from_str(&event_data.gender).unwrap()); // IMPROVE: implement (de)serializing genders
            let name = PersonName::new(
                event_data.naming.first_name,
                event_data.naming.prefix,
                event_data.naming.family_name,
            );
            let address_ref = AddressReference(event_data.address_registration.address);

            // Mark the seeded BSN as used
            bsn_state.mark_as_used(bsn.0);

            // Create components
            let rng = RngComponent::from(&mut global_rng);
            let age = Age::new(birth_date);
            let id = Id { uuid: subject };

            let mut person = PersonBundle {
                person: Person,
                rng,
                age,
                id,
            };

            // Create entitiy
            let can_be_pregnant = person.rng.bool(); // Assume 50% of the people can become pregnant. IMPROVE: make more realistic, based on statistics?
            let mut entity_cmd = commands.spawn((person, bsn, gender, name, address_ref, FromSeed));
            if can_be_pregnant {
                entity_cmd.insert(CanBePregnant);
            }

            let entity = entity_cmd.id();

            // Update state
            seed_state.push(subject, entity);

            // Finish
            message.ack().unwrap();
            message_count += 1;
        }

        if message_count == 0 {
            info!("Load `seed data birth` end");
            break;
        }
    }
}

pub fn load_seed_addresses(
    mut commands: Commands,
    nats_state: Res<NatsState>,
    mut address_state: ResMut<AddressState>,
) {
    info!("Load `seed data address` start");

    let stream = get_seed_stream(&nats_state.uri, ADDRESS_TOPIC);

    loop {
        let messages = stream.fetch(BatchOptions {
            batch: 100,
            expires: None,
            no_wait: true,
        });

        let messages = match messages {
            Ok(m) => m,
            Err(e) => {
                error!("Error loading events batch {}", e);
                break;
            }
        };

        let mut message_count = 0;
        for message in messages {
            // Parse data
            let data = std::str::from_utf8(&message.data).unwrap();
            let event: Event = serde_json::from_str(data).unwrap();
            let event_data: EventAddressRegistationData =
                serde_json::from_value(event.event_data).unwrap();
            let subject = event.subject_ids[0]; // Assume the event always contains a subject ID (which should be the case)

            // Create entitiy
            let addr = Address::new(
                subject,
                (
                    event_data.municipality.code.to_string(),
                    event_data.municipality.name.to_string(),
                ),
                event_data.purpose.0.to_string(),
            );

            let entity = commands.spawn((addr, FromSeed)).id();

            // Update state
            address_state.push(subject, entity);

            // Finish
            message.ack().unwrap();
            message_count += 1;
        }

        if message_count == 0 {
            info!("Load `seed data address` end");
            break;
        }
    }
}

pub fn load_seed_deaths(
    mut commands: Commands,
    nats_state: Res<NatsState>,
    seed_state: Res<SeedState>,
) {
    info!("Load `seed data death` start");

    let stream = get_seed_stream(&nats_state.uri, DEATH_TOPIC);

    loop {
        let messages = stream.fetch(BatchOptions {
            batch: 100,
            expires: None,
            no_wait: true,
        });

        let messages = match messages {
            Ok(m) => m,
            Err(e) => {
                error!("Error loading events batch {}", e);
                break;
            }
        };

        let mut message_count = 0;
        for message in messages {
            // Parse data
            let data = std::str::from_utf8(&message.data).unwrap();
            let event: Event = serde_json::from_str(data).unwrap();
            let subject = event.subject_ids[0];

            // Find entity
            let entity = seed_state.get(&subject).unwrap();

            // Remove entity
            commands.entity(entity).despawn();

            // Finish
            message.ack().unwrap();
            message_count += 1;
        }

        if message_count == 0 {
            info!("Load `seed data death` end");
            break;
        }
    }
}

pub fn inspect_seeded(
    mut query: Query<(&Id, &Age, Option<&PersonName>), With<FromSeed>>,
    clock_state: Res<ClockState>,
) {
    info!("inspect_seeded");

    let count = query.iter().count();
    for (id, age, name) in query.iter_mut() {
        match name {
            Some(name) => info!(
                "Found seeded person with id: {}, name: {}, age: {} years",
                id.uuid,
                name,
                age.age(clock_state.get_time()).num_days() / 365
            ),
            None => warn!("Found seeded person without name: {}", id.uuid),
        };
    }

    info!("inspect_seeded: total found {}", count);
}
