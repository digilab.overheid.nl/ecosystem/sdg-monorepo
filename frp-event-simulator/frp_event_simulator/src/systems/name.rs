use bevy::prelude::*;
use bevy_turborand::prelude::*;
use sdg_event_simulator_common::bevy::resources::{clock_state::ClockState, nats_state::NatsState};

use crate::{
    components::*,
    event::{name_change_event, NAME_TOPIC},
};

#[allow(clippy::type_complexity)]
pub fn sim_name_change(
    mut commands: Commands,
    mut query: Query<
        (Entity, &mut RngComponent, &Id, &PersonName, &Gender),
        (With<Person>, With<PersonName>),
    >,
    clock_state: Res<ClockState>,
    nats_state: Res<NatsState>,
) {
    for (entity, mut rng, id, name, gender) in query.iter_mut() {
        let is_first_name_changed =
            PersonName::event_change_first_name_occurs(&mut rng, &clock_state);
        let is_family_name_changed =
            PersonName::event_change_family_name_occurs(&mut rng, &clock_state);

        if !is_first_name_changed && !is_family_name_changed {
            continue;
        }

        // Else...
        // IMPROVE: since the probability to reach this line is very low, benchmark if it is more efficient to select only the entity ID first and do a new query here to select the name and gender

        let mut first_name: String;
        let mut prefix: String;
        let mut family_name: String;

        if is_first_name_changed {
            // Randomly generate a new first name and repeat if the name equals the existing name
            first_name = random_first_name(&gender.0, &mut rng);
            while first_name == name.first_name {
                first_name = random_first_name(&gender.0, &mut rng);
            }
        } else {
            first_name = name.first_name.clone();
        }

        if is_family_name_changed {
            // Randomly generate a new family name with prefix and repeat if the name equals the existing name
            (prefix, family_name) = random_family_name(&mut rng);
            while prefix == name.prefix && family_name == name.family_name {
                (prefix, family_name) = random_family_name(&mut rng);
            }
        } else {
            prefix = name.prefix.clone();
            family_name = name.family_name.clone();
        }

        debug!(
            time = clock_state.get_time().to_string(),
            eid = entity.index(),
            first_name = first_name,
            prefix = prefix,
            family_name = family_name,
            "NAME    "
        );

        let entity_cmd = commands.get_entity(entity);

        if let Some(mut entity_cmd) = entity_cmd {
            let message = name_change_event(
                &mut rng,
                &clock_state.get_time(),
                id.uuid,
                &first_name,
                &prefix,
                &family_name,
            );

            entity_cmd.insert(PersonName::new(first_name, prefix, family_name));

            nats_state
                .connection
                .publish(NAME_TOPIC, message.to_string())
                .unwrap();
        }
    }
}
