use crate::{
    components::GenderValue,
    event::{CountryOfBirth, PlaceOfBirth},
};
use bevy::prelude::*;
use bevy_turborand::prelude::*;
use sdg_event_simulator_common::bevy::resources::{clock_state::ClockState, nats_state::NatsState};

use crate::{
    components::*,
    event::{
        birth_registration_event,
        BIRTH_TOPIC,
    },
    AddressState, BsnState,
};

#[allow(clippy::type_complexity)]
pub fn sim_birth_registration(
    mut commands: Commands,
    mut query: Query<(Entity, &mut RngComponent, &Id, &Age, &Parent), (With<Person>, Without<BSN>)>,
    name_query: Query<&PersonName>,
    parent_addr_ref_query: Query<&AddressReference>,
    addr_query: Query<&Address>,
    clock_state: Res<ClockState>,
    nats_state: Res<NatsState>,
    mut bsn_state: ResMut<BsnState>,
    address_state: Res<AddressState>,
) {
    for (entity, mut rng, id, age, parent) in query.iter_mut() {
        // Note: at birth registration, the following components are assigned to a person entity: BSN, Name, Gender, and Address
        if !age.event_birth_registration_occurs(&mut rng, &clock_state) {
            continue;
        }

        let now = clock_state.get_time();

        // Generate a BSN
        let bsn = bsn_state.generate_new(&mut rng);

        debug!(
            time = clock_state.get_time().to_string(),
            eid = entity.index(),
            bsn = bsn,
            "BSN     "
        );

        // Set a gender. Note: at birth, we assume gender is either Female or Male, as https://www.cbs.nl/en-gb/visualisations/dashboard-population/men-and-women seems to suggest
        let mut gender = GenderValue::Male;
        if Gender::event_female_at_birth_occurs(&mut rng, &clock_state) {
            gender = GenderValue::Female;
        }

        debug!(
            time = now.to_string(),
            f_id = entity.index(),
            gender = gender.to_string(),
            "GENDER  "
        );

        info!(
            "birth registered, age of the person: {} days. generated BSN: {}, gender: {}",
            age.age(now).num_days() as f64,
            bsn,
            gender,
        );

        // Get the parent entity
        let parent_entity: Entity = parent.get();

        // Generate a name
        let mut prefix = String::new();
        let family_name: String;
        let parent_name = name_query.get_component::<PersonName>(parent_entity);
        match parent_name {
            Ok(result) => {
                prefix = result.prefix.clone();
                family_name = result.family_name.clone();
            }
            Err(_err) => {
                family_name = format!("O #{}", entity.index());
            }
        }

        let mut first_name = random_first_name(&gender, &mut rng);

        let mut data_error: Option<DataError> = None;
        if  DataError::event_misspell_first_name_occurs(&mut rng) {
            data_error = Some(DataError {attribute: "firstName".to_string(), intended_value: first_name.clone()}); // Store the intended value
            first_name = DataError::manipulate_string(first_name, &mut rng);
        }

        debug!(
            time = clock_state.get_time().to_string(),
            eid = entity.index(),
            first_name = first_name,
            prefix = prefix,
            family_name = family_name,
            "NAME    "
        );

        // Give the child the same address as the mother (parent). IMPROVE: make sure the 'parent' refers to the mother
        let addr_ref = parent_addr_ref_query
            .get_component::<AddressReference>(parent_entity)
            .expect("parent address reference not found");
        let addr_entity = address_state
            .get(&addr_ref.0)
            .expect("referenced address not found in state");
        let addr = addr_query
            .get_component::<Address>(addr_entity)
            .expect("referenced address not found");

        debug!(
            time = now.to_string(),
            eid = entity.index(),
            aid = addr_ref.to_string(),
            "ADDRESS "
        );

        let entity_cmd = commands.get_entity(entity);

        if let Some(mut entity_cmd) = entity_cmd {
            entity_cmd
                .insert(BSN(bsn))
                .insert(Gender(gender))
                .insert(PersonName::new(
                    first_name.clone(),
                    prefix.clone(),
                    family_name.clone(),
                ))
                .insert(addr_ref.clone()); // Let the child be registered at the same address as the parent

            if data_error.is_some() {
                entity_cmd.insert(data_error.unwrap());
            }

            let message = birth_registration_event(
                &mut rng,
                &now,
                id.uuid,
                bsn,
                &gender,
                age,
                first_name,
                prefix,
                family_name,
                addr_ref.0,
                // IMPROVE: allow different places of birth (e.g. cities with hospitals) than the first registration address
                PlaceOfBirth {
                    code: addr.municipality.0.to_owned(),
                    name: addr.municipality.1.to_owned(),
                },
                CountryOfBirth {
                    code: "6030".to_string(),
                    name: "Nederland".to_string(),
                },
            );

            nats_state
                .connection
                .publish(BIRTH_TOPIC, message.to_string())
                .unwrap();
        }
    }
}

#[allow(clippy::type_complexity)]
pub fn sim_pregnancy(
    mut commands: Commands,
    mut query: Query<
        (&mut RngComponent, Entity, &PersonName, &Age),
        (With<Person>, With<CanBePregnant>, Without<IsPregnant>),
    >,
    clock_state: Res<ClockState>,
) {

    for (mut rng, entity, name, age) in query.iter_mut() {
        let now = clock_state.get_time();
        if age.event_pregnancy_occurs(&mut rng, &clock_state) {
            // Output some information
            info!(
                "person pregnant, age of mother: {} years",
                age.age(now).num_days() / 365
            );

            debug!(
                time = now.to_string(),
                f_id = entity.index(),
                name = name.family_name,
                "PREGNANT"
            );

            let entity_cmd = commands.get_entity(entity);

            if let Some(mut entity_cmd) = entity_cmd {
                entity_cmd.insert(IsPregnant { start: now });
            }
        }
    }
}

#[allow(clippy::type_complexity)]
pub fn sim_birth(
    mut commands: Commands,
    mut global_rng: ResMut<GlobalRng>,
    mut query: Query<(Entity, &mut RngComponent, &PersonName, &IsPregnant), With<Person>>,
    clock_state: Res<ClockState>,
    // nats_state: Res<NatsState>,
) {
    for (entity, mut _rng, name, preg) in query.iter_mut() {
        let now = clock_state.get_time();

        if preg.is_complete(now) {
            let mut child_person = PersonBundle::new(&mut global_rng, now);
            // let child_uuid = child_person.id.uuid;

            let child = if child_person.rng.bool() {
                commands.spawn((child_person, CanBePregnant)).id()
            } else {
                commands.spawn(child_person).id()
            };
            debug!(
                time = now.to_string(),
                parent_entity = entity.index(),
                name = name.family_name,
                cid = child.index(),
                "BIRTH   "
            );

            // let message = birth_event(&mut rng, &now, child_uuid, bsn, gender);

            // nats_state
            //     .connection
            //     .publish(BIRTH_TOPIC, message.to_string())
            //     .unwrap();

            commands.entity(entity).push_children(&[child]);
            commands.entity(entity).remove::<IsPregnant>();
        }
    }
}
