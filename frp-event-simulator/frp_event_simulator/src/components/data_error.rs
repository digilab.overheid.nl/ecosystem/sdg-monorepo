use bevy::prelude::*;
use bevy_turborand::{DelegatedRng, RngComponent};
use chrono::Duration;
use sdg_event_simulator_common::bevy::resources::clock_state::ClockState;
use sdg_event_simulator_common::chance::PercentageChance;

// DataError is a component to define errors in data, e.g. spelling mistakes
#[derive(Component, Clone, Debug)]
pub struct DataError {
    pub attribute: String,      // E.g. firstName
    pub intended_value: String, // Note: can be stored in JSON format if needed
}

impl DataError {
    // Spelling errors sometimes occur and can be corrected by the 'ambtenaar van de burgerlijke stand', see https://www.rijksoverheid.nl/onderwerpen/aangifte-geboorte-en-naamskeuze-kind/vraag-en-antwoord/spelfout-verbeteren-geboorteakte. Since there seems to be little data publicly available about how often this happens, we assume a probability of 1% per birth registration
    pub fn event_misspell_first_name_occurs(rng: &mut RngComponent) -> bool {
        let rand = rng.u64(0..u64::MAX);
        let chance = PercentageChance::new(1.0).fraction_of_u64();

        rand < chance
    }

    // Assume the probability that a data error is detected and corrected is 50% per month
    pub fn event_correction_occurs(rng: &mut RngComponent, clock_state: &ClockState) -> bool {
        let rand = rng.u64(0..u64::MAX);
        let chance = PercentageChance::new(50.0)
            .scale(Duration::days(365 / 12), clock_state.time_per_tick)
            .fraction_of_u64();

        rand < chance
    }

    // manipulate_string returns the input string with a random modification
    pub fn manipulate_string(input: String, rng: &mut RngComponent) -> String {
        if input.is_empty() {
            return input;
        }

        let action: u8 = rng.u8(0..=3);
        let position: usize = rng.usize(0..input.len() - 1);
        let replacement: char = rng.alphanumeric();

        let mut chars: Vec<char> = input.chars().collect();

        // Choose one of the following actions randomly
        match action {
            // Replace a character
            0 => {
                chars[position] = replacement;
            }

            // Remove a character
            1 => {
                chars.remove(position);
            }

            // Swap a character with the next one
            2 => {
                let len = chars.len();
                chars.swap(position, (position + 1) % len);
            }

            // Insert a new character
            3 => {
                chars.insert(position, replacement);
            }

            _ => {} // This should never happen
        }

        chars.iter().collect()
    }
}
