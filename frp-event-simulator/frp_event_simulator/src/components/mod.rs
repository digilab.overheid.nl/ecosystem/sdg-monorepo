use bevy::prelude::*;
use bevy_turborand::prelude::*;
use chrono::NaiveDateTime;

mod address;
mod age;
mod bsn;
mod data_error;
mod gender;
mod id;
mod person;
mod person_name;
mod pregnant;

pub use address::{Address, AddressReference};
pub use age::Age;
pub use bsn::BSN;
pub use data_error::DataError;
pub use gender::{Gender, GenderValue};
pub use id::Id;
pub use person::Person;
pub use person_name::{random_family_name, random_first_name, PersonName};
pub use pregnant::{CanBePregnant, IsPregnant};

#[derive(Bundle)]
pub struct PersonBundle {
    pub person: Person,
    pub rng: RngComponent,
    pub age: Age,
    pub id: Id,
}

impl PersonBundle {
    pub fn new(global_rng: &mut ResMut<GlobalRng>, time: NaiveDateTime) -> Self {
        let mut rng = RngComponent::from(global_rng);

        let age = Age::new(time);
        let id = Id::new(&mut rng);

        Self {
            person: Person,
            rng,
            age,
            id,
        }
    }
}
