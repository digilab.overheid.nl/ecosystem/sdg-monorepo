use bevy::prelude::*;
use chrono::{Duration, NaiveDateTime};

#[derive(Component)]
pub struct CanBePregnant;

#[derive(Component)]
pub struct IsPregnant {
    pub start: NaiveDateTime,
}

impl IsPregnant {
    pub fn is_complete(&self, now: NaiveDateTime) -> bool {
        self.start
            .checked_add_signed(Duration::days(9 * 30))
            .unwrap()
            < now
    }
}
