use super::GenderValue;
use crate::data::names::{FAMILY_NAMES, FIRST_NAMES_FEMALE, FIRST_NAMES_MALE, FIRST_NAMES_X};
use bevy::prelude::*;
use bevy_turborand::{DelegatedRng, RngComponent};
use chrono::Duration;
use sdg_event_simulator_common::{
    bevy::resources::clock_state::ClockState, chance::PercentageChance,
};
use std::fmt::Display;

#[derive(Component, Clone, Debug)]
pub struct PersonName {
    pub first_name: String,
    pub prefix: String, // Note: 'voorvoegsel' in Dutch, which can actually consist of multiple prefixes
    pub family_name: String, // Note: 'geslachtsnaam' in Dutch
}

impl Display for PersonName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let family_name_with_prefix = if self.prefix.is_empty() {
            self.family_name.clone()
        } else {
            format!("{} {}", self.prefix, self.family_name)
        };

        write!(f, "{} {}", self.first_name, family_name_with_prefix)
    }
}

impl PersonName {
    pub fn new(first_name: String, prefix: String, family_name: String) -> Self {
        Self {
            first_name,
            prefix,
            family_name,
        }
    }

    // event_change_first_name_occurs randomly returns whether a person's first name will be changed. Sources: https://www.rijksoverheid.nl/onderwerpen/privacy-en-persoonsgegevens/vraag-en-antwoord/voornaam-of-achternaam-veranderen and https://www.rechtspraak.nl/Onderwerpen/Wijzigen-voornaam. According to https://www.rechtspraak.nl/Organisatie-en-contact/Organisatie/Raad-voor-de-rechtspraak/Nieuws/Paginas/Voornaamswijzigingen-toegenomen-2018.aspx, ~600 requests for first name changes are done yearly in a population of ~17.2 million people and almost all requests are approved, which corresponds to a probability of ~0.0035% per person per year. Note: a first name can also be changed simultaneously with a gender change (source: https://www.rvr.org/kenniswijzer/zoeken-kenniswijzer/toevoegen/personen/p080/#voornaamswijziging-bij-geslachtswijziging). This seems to be normally done without court case, and these cases seem to be not included in the above numbers.
    pub fn event_change_first_name_occurs(rng: &mut RngComponent, time: &ClockState) -> bool {
        let rand = rng.u64(0..u64::MAX);
        let chance = PercentageChance::new(0.0035)
            .scale(Duration::days(365), time.time_per_tick)
            .fraction_of_u64();

        rand < chance
    }

    // event_change_family_name_occurs randomly returns whether a person's family (last) name will be changed. According to https://www.justis.nl/producten/naamswijziging/kwartaalcijfers-2023-naamswijziging, ~3169 requests for family name changes are approved per year for a population of ~17.85 million people, which corresponds to a probability of ~0.0178% per person per year
    pub fn event_change_family_name_occurs(rng: &mut RngComponent, time: &ClockState) -> bool {
        let rand = rng.u64(0..u64::MAX);
        let chance = PercentageChance::new(0.0178)
            .scale(Duration::days(365), time.time_per_tick)
            .fraction_of_u64();

        rand < chance
    }
}

pub fn random_first_name(gender: &GenderValue, rng: &mut RngComponent) -> String {
    match gender {
        GenderValue::Female => rng.sample(FIRST_NAMES_FEMALE).unwrap().to_string(),
        GenderValue::Male => rng.sample(FIRST_NAMES_MALE).unwrap().to_string(),
        GenderValue::X => rng.sample(FIRST_NAMES_X).unwrap().to_string(),
    }
}

pub fn random_family_name(rng: &mut RngComponent) -> (String, String) {
    let total = f64::from(
        FAMILY_NAMES
            .iter()
            .map(|&(_, _, weight)| weight)
            .sum::<u32>(),
    );

    let (prefix, family_name, _) = rng
        .weighted_sample(FAMILY_NAMES, |((_, _, weight), _)| (*weight as f64) / total)
        .unwrap();
    (prefix.to_string(), family_name.to_string())
}
