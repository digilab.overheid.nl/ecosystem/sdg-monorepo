# available variables:
# - $ts - Current unix timestamp, used to make entities unique where needed.
# - $world_id - id of newly created world. Do not delete this world, because other tests may depend on it.

echo 'Get specific world:'
resp=$(curl $CURL_FLAGS \
    -H 'Accept: application/json' \
    "http://otherdevorg-world-backend-127.0.0.1.nip.io:8080/v0/worlds/$world_id" \
)
printf '%s' "$resp" | jq .

echo 'Search worlds:'
resp=$(curl $CURL_FLAGS \
    -H 'Accept: application/json' \
    "http://otherdevorg-world-backend-127.0.0.1.nip.io:8080/v0/worlds?name=$ts" \
)
printf '%s' "$resp" | jq .
