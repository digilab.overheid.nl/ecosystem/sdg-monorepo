# available variables:
# - $ts - Current unix timestamp, used to make entities unique where needed.
# - $world_id - id of newly created world. Do not delete this world, because other tests may depend on it.

echo 'Get greeting for world:'
resp=$(curl $CURL_FLAGS \
    -H 'Accept: application/json' \
    "http://mydevorg-hello-backend-127.0.0.1.nip.io:8080/v0/greetings/$world_id?emphasis=true" \
)
printf '%s' "$resp" | jq .
