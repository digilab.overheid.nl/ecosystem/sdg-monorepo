package cmd

import (
	"log/slog"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-backend/application"
	"gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-backend/application/repository/claim"
	"gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-backend/config"
)

var serveOpts struct { //nolint:gochecknoglobals // this is the recommended way to use cobra
}

var serveCommand = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "serve",
	Short: "Start the api",
	Run: func(cmd *cobra.Command, args []string) {
		cfg, err := config.New()
		if err != nil {
			slog.Error("config new failed", "err", err)
			return
		}

		var logLevel slog.Level
		if cfg.Debug {
			logLevel = slog.LevelDebug
		} else {
			logLevel = slog.LevelInfo
		}

		logger := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
			Level: logLevel,
		})).With("application", "http_server")

		logger.Info("Starting fictief brp backend", "config", cfg, "loglevel", logLevel)

		repository, err := claim.New(logger, cfg.Repository.Claim)
		if err != nil {
			logger.Error("failed to create new static repository", "err", err)
			return
		}

		app := application.New(logger, cfg, repository)

		app.Router()

		if err := app.ListenAndServe(); err != nil {
			logger.Error("listen and serve failed", "err", err)
			return
		}

		os.Exit(0)
	},
}
