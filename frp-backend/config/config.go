package config

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/iamolegga/enviper"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v3"
)

type Config struct {
	Debug         bool
	ListenAddress string
	Repository    RepositoryConfig
}

type RepositoryConfig struct {
	Static RepositoryStaticConfig
	Claim  RepositoryClaimConfig
}

type RepositoryStaticConfig struct {
	DatasetPath string
}

type RepositoryClaimConfig struct {
	Server string
}

// New composes a config with values from the specified paths
func New() (*Config, error) {
	e := enviper.New(viper.New())
	e.SetEnvPrefix("APP")

	config := new(Config)
	if err := e.Unmarshal(config); err != nil {
		panic("unable to decode into config struct")
	}

	return config, nil
}

func getPathAndFile(path string) (string, string) {
	p := filepath.Dir(path)
	base := filepath.Base(path)
	ext := filepath.Ext(base)
	file, _ := strings.CutSuffix(base, ext)

	return p, file
}

func readConfig(path string, cfg any) error {
	file, err := os.ReadFile(path)
	if err != nil {
		return fmt.Errorf("error reading config file: %w", err)
	}

	if err := yaml.Unmarshal(file, cfg); err != nil {
		return fmt.Errorf("unmarshalling config failed: %w", err)
	}

	return nil
}
