package claim

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"time"

	"github.com/google/uuid"
	claimAPI "gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/claim-api"
	api "gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-backend/application/repository/claim/field"
	"gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-backend/config"
)

type Claim struct {
	client *claimAPI.ClientWithResponses
	logger *slog.Logger
}

var ErrNotFound = errors.New("not found")

func (c *Claim) findSubjectIDBasedOnBSN(ctx context.Context, bsn string) (claimAPI.SubjectID, error) {
	claimType := "Burgerservicenummer"
	t := time.Now()

	// Convert the BSN to a JSON-encoded object
	dataJSON, _ := json.Marshal(struct {
		Value string `json:"value"`
	}{bsn})
	data := string(dataJSON)

	params := &claimAPI.FindClaimParams{
		ClaimType:         &claimType,
		RegistrationStart: &t,
		RegistrationEnd:   &t,
		ValidStart:        &t,
		ValidEnd:          &t,
		Data:              &data,
	}

	response, err := c.client.FindClaimWithResponse(ctx, params)
	if err != nil {
		return uuid.Nil, fmt.Errorf("find claim with response: %w", err)
	}

	if response.JSON200 == nil {
		return uuid.Nil, fmt.Errorf("client find claim: %w", errors.New(string(response.Body)))
	}

	if len(*response.JSON200.Data) == 0 {
		return uuid.Nil, ErrNotFound
	}

	if len(*response.JSON200.Data) > 1 {
		return uuid.Nil, fmt.Errorf("found multiple claims, should not be possible to have multiple person for one BSN")
	}

	return (*response.JSON200.Data)[0].Subject, nil
}

func (c *Claim) RaadpleegAllePersonen(ctx context.Context, request api.GetPersonen) (*api.GetPersonenQueryResponse, error) {
	claimType := "Burgerservicenummer"
	t := time.Now()

	params := &claimAPI.FindClaimParams{
		ClaimType:         &claimType,
		RegistrationStart: &t,
		RegistrationEnd:   &t,
		ValidStart:        &t,
		ValidEnd:          &t,
	}

	response, err := c.client.FindClaimWithResponse(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("find claim with response: %w", err)
	}

	if response.StatusCode() == http.StatusNotFound {
		return nil, nil
	}

	if response.JSON200 == nil {
		return nil, fmt.Errorf("client find claim: %w", errors.New(string(response.Body)))
	}

	fields := field.New(request.Fields)

	personen := make([]api.Persoon, 0, len(*response.JSON200.Data))

	offset := 0
	if request.Offset != nil {
		offset = *request.Offset
	}

	limit := 10
	if request.Limit != nil {
		limit = *request.Limit
	}

	for idx := offset * limit; idx < (offset+1)*limit && idx < len(*response.JSON200.Data); idx++ {
		value := (*response.JSON200.Data)[idx]

		c.logger.Debug("Persoon ID", "ID", value.Subject)

		persoon, err := c.getPersoon(ctx, value.Subject, fields)
		if err != nil {
			return nil, fmt.Errorf("get persoon: %w", err)
		}

		if fields.Contains("burgerservicenummer") {
			bytes, err := json.Marshal(value.Data)
			if err != nil {
				return nil, fmt.Errorf("data marshal: %w", err)
			}

			var data struct {
				Value string `json:"value"`
			}

			if err := json.Unmarshal(bytes, &data); err != nil {
				return nil, fmt.Errorf("data unmarshal: %w", err)
			}

			persoon.Burgerservicenummer = &data.Value
		}

		personen = append(personen, persoon)
	}

	return &api.GetPersonenQueryResponse{
		Metadata: &api.Metadata{
			Limit:      limit,
			Offset:     offset,
			TotalCount: len(*response.JSON200.Data),
		},
		Personen: &personen,
	}, nil
}

// RaadpleegMetBurgerservicenummer implements repository.Repository.
func (c *Claim) RaadpleegMetBurgerservicenummer(ctx context.Context, params api.RaadpleegMetBurgerservicenummer) ([]api.Persoon, error) {
	personen := make([]api.Persoon, 0, len(params.Burgerservicenummer))

	fields := field.New(params.Fields)

	for _, bsn := range params.Burgerservicenummer {
		id, err := c.findSubjectIDBasedOnBSN(ctx, bsn)
		if err != nil {
			if errors.Is(err, ErrNotFound) {
				continue
			}
			return nil, fmt.Errorf("find subject id based on bsn: %w", err)
		}

		c.logger.Debug("Persoon ID", "ID", id)

		persoon, err := c.getPersoon(ctx, id, fields)
		if err != nil {
			return nil, fmt.Errorf("get persoon: %w", err)
		}

		if fields.Contains("burgerservicenummer") {
			persoon.Burgerservicenummer = &bsn
		}

		personen = append(personen, persoon)
	}

	return personen, nil
}

// ZoekMetGeslachtsnaamEnGeboorteDatum implements repository.Repository.
func (c *Claim) ZoekMetGeslachtsnaamEnGeboorteDatum(ctx context.Context, params api.ZoekMetGeslachtsnaamEnGeboortedatum) ([]api.PersoonBeperkt, error) {
	panic("unimplemented")
}

// ZoekMetNaamEnGemeenteVanInschrijving implements repository.Repository.
func (c *Claim) ZoekMetNaamEnGemeenteVanInschrijving(ctx context.Context, params api.ZoekMetNaamEnGemeenteVanInschrijving) ([]api.PersoonBeperkt, error) {
	panic("unimplemented")
}

// ZoekMetNummeraanduidingIdentificatie implements repository.Repository.
func (c *Claim) ZoekMetNummeraanduidingIdentificatie(ctx context.Context, params api.ZoekMetNummeraanduidingIdentificatie) ([]api.PersoonBeperkt, error) {
	panic("unimplemented")
}

// ZoekMetPostcodeEnHuisnummer implements repository.Repository.
func (c *Claim) ZoekMetPostcodeEnHuisnummer(ctx context.Context, params api.ZoekMetPostcodeEnHuisnummer) ([]api.PersoonBeperkt, error) {
	panic("unimplemented")
}

// ZoekMetStraatHuisnummerEnGemeenteVanInschrijving implements repository.Repository.
func (c *Claim) ZoekMetStraatHuisnummerEnGemeenteVanInschrijving(ctx context.Context, params api.ZoekMetStraatHuisnummerEnGemeenteVanInschrijving) ([]api.PersoonBeperkt, error) {
	panic("unimplemented")
}

func New(logger *slog.Logger, cfg config.RepositoryClaimConfig) (*Claim, error) {
	client, err := claimAPI.NewClientWithResponses(cfg.Server)
	if err != nil {
		return nil, err
	}

	return &Claim{
		client: client,
		logger: logger,
	}, nil
}
