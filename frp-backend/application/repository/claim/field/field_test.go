package field_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	api "gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-backend/application/repository/claim/field"
)

func TestContains(t *testing.T) {
	fields := field.New([]api.Field{
		"naam",
	})

	assert.True(t, fields.Contains("naam"))
	assert.True(t, fields.Contains("naam.voornamen"))
	assert.True(t, fields.Contains("naam.aanduidingNaamgebruik.code"))
	assert.False(t, fields.Contains("geboorte"))
}

func TestNeeds(t *testing.T) {
	fields := field.New([]api.Field{
		"naam.voornamen",
	})

	assert.True(t, fields.Needs("naam"))
	assert.True(t, fields.Needs("naam.voornamen"))
	assert.True(t, fields.Needs("naam.voornamen.x"))
	assert.False(t, fields.Needs("naam.geslachtsnaam"))
	assert.False(t, fields.Needs("geboorte"))

	fields = field.New([]api.Field{
		"geboorte.datum.datum",
	})
	assert.True(t, fields.Needs("geboorte.datum"))
}
