package field

import (
	"strings"

	api "gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-api"
)

type Fields map[string]struct{}

func New(fields []api.Field) Fields {
	mapFields := make(Fields, len(fields))

	for _, field := range fields {
		mapFields[field] = struct{}{}
	}

	return mapFields
}

func (fields Fields) Needs(key api.Field) bool {
	for field := range fields {
		if fields.Contains(key) {
			return true
		}

		substr := strings.Split(field, ".")

		k := ""
		for _, str := range substr {
			k += str

			if k == key {
				return true
			}
			k += "."
		}
	}

	return false
}

func (fields Fields) Contains(key api.Field) bool {
	keys := strings.Split(key, ".")

	k := ""
	for _, key := range keys {
		k += key
		if _, ok := fields[k]; ok {
			return true
		}
		k += "."
	}

	return false
}
