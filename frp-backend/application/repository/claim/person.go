package claim

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/goodsign/monday"

	"github.com/oapi-codegen/runtime/types"
	claimAPI "gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/claim-api"
	api "gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-backend/application/repository/claim/field"
)

type Date time.Time

// Implement Marshaler and Unmarshaler interface
func (d *Date) UnmarshalJSON(b []byte) error {
	s := strings.Trim(string(b), "\"")
	t, err := time.Parse("2006-01-02", s)
	if err != nil {
		return err
	}
	*d = Date(t)
	return nil
}

func (d Date) MarshalJSON() ([]byte, error) {
	return json.Marshal(time.Time(d))
}

// Maybe a Format function for printing your date
func (d Date) Format(s string) string {
	t := time.Time(d)
	return t.Format(s)
}

// stringPointer returns a pointer to the specified string
func stringPointer(str string) *string {
	return &str
}

func (c *Claim) getPersoon(ctx context.Context, id claimAPI.SubjectID, fields field.Fields) (api.Persoon, error) {
	var persoon api.Persoon
	var err error

	if fields.Needs("naam") {
		persoon.Naam, err = c.getPersoonClaimNaam(ctx, id, fields)
		if err != nil {
			fmt.Printf("'naam' requested for person with ID %s that does not have this attribute: %v\n", id, err)
			// Note: do not return when not found, since there is possibly no claim about this attribute
		}
	}

	if fields.Needs("geslacht") {
		var data struct {
			Value string `json:"value"`
		}
		err = c.getPersoonClaim(ctx, id, "Geslacht", &data)
		if err != nil {
			fmt.Printf("'geslacht' requested for person with ID %s that does not have this attribute: %v\n", id, err)
			// Note: do not return when not found, since there is possibly no claim about this attribute
		}

		persoon.Geslacht = &api.Waardetabel{
			Code: &data.Value,
			// Note: Omschrijving seems to be left empty in the HaalCentraal API
		}
	}

	if fields.Needs("geboorte") || fields.Needs("leeftijd") {
		persoon.Geboorte, persoon.Leeftijd, err = c.getPersoonClaimGeboorte(ctx, id, fields)
		if err != nil {
			fmt.Printf("'geboorte' requested for person with ID %s that does not have this attribute: %v\n", id, err)
			// Note: do not return when not found, since there is possibly no claim about this attribute
		}
	}

	if fields.Needs("adressering") || fields.Needs("gemeenteVanInschrijving") {
		type Municipality struct {
			Code string `json:"code"`
			Name string `json:"name"`
		}

		var data struct {
			Street              string       `json:"street"`
			HouseNumber         int          `json:"houseNumber"`
			HouseNumberAddition string       `json:"houseNumberAddition"`
			HouseLetter         string       `json:"houseLetter"`
			PostalCode          string       `json:"postalCode"`
			Municipality        Municipality `json:"municipality"`
			Surface             int          `json:"surface"`
			Purpose             string       `json:"purpose"`
		}
		if err := c.getPersoonClaim(ctx, id, "Adressering", &data); err != nil {
			if !errors.Is(err, ErrNotFound) {
				fmt.Printf("'adressering' or 'gemeenteVanInschrijving' requested for person with ID %s that does not have this attribute: %v\n", id, err)
				// Note: do not return when not found, since there is possibly no claim about this attribute
			}
		}

		if fields.Needs("adressering") {
			persoon.Adressering = &api.Adressering{
				Adresregel1: stringPointer(fmt.Sprintf("%s %d %s %s", data.Street, data.HouseNumber, data.HouseLetter, data.HouseNumberAddition)), // IMPROVE: improve concatenation, e.g. append dash if both house letter and addition, remove duplicate spaces
				Adresregel2: stringPointer(fmt.Sprintf("%s %s", data.PostalCode, data.Municipality.Name)),
				// Note: Adresregel3 left empty
				Land: &api.Waardetabel{
					// Note: we assume all people in the BRP are registered on a NL address
					Code:         stringPointer("6030"),
					Omschrijving: stringPointer("Nederland"),
				},
			}
		}

		if fields.Needs("gemeenteVanInschrijving") {
			persoon.GemeenteVanInschrijving = &api.Waardetabel{
				Code:         &data.Municipality.Code,
				Omschrijving: &data.Municipality.Name,
			}
		}
	}

	if fields.Needs("overlijden") {
		persoon.Overlijden, err = c.getPersoonClaimOverlijden(ctx, id, fields)
		if err != nil {
			fmt.Printf("'overlijden' requested for person with ID %s that does not have this attribute: %v\n", id, err)
			// Note: do not return when not found, since there is possibly no claim about this attribute
		}
	}

	return persoon, nil
}

// getPersoonClaim fetches the specified claim and unmarshals the result claim to the specified pointer
func (c *Claim) getPersoonClaim(ctx context.Context, id claimAPI.SubjectID, claim string, v any) error {
	t := time.Now()

	params := &claimAPI.FindClaimParams{
		ClaimSubject:      &id,
		ClaimType:         &claim,
		RegistrationStart: &t,
		RegistrationEnd:   &t,
		ValidStart:        &t,
		ValidEnd:          &t,
	}

	response, err := c.client.FindClaimWithResponse(ctx, params)
	if err != nil {
		return err
	}

	if response.JSON200 == nil {
		return fmt.Errorf("client find claim: %w", errors.New(string(response.Body)))
	}

	if len(*response.JSON200.Data) == 0 {
		return ErrNotFound
	}

	if len(*response.JSON200.Data) > 1 {
		return fmt.Errorf("multiple data points, cannot determine the truth")
	}

	bytes, err := json.Marshal((*response.JSON200.Data)[0].Data)
	if err != nil {
		return fmt.Errorf("data marshal: %w", err)
	}

	if err := json.Unmarshal(bytes, v); err != nil {
		return fmt.Errorf("data unmarshal: %w", err)
	}

	return nil
}

func (c *Claim) getPersoonClaimNaam(ctx context.Context, id claimAPI.SubjectID, fields field.Fields) (*api.NaamPersoon, error) {
	naam := &api.NaamPersoon{}

	if fields.Contains("naam") || fields.Contains("naam.voornamen") || fields.Contains("naam.voorletters") {
		var data struct {
			Value string `json:"value"`
		}
		if err := c.getPersoonClaim(ctx, id, "Voornamen", &data); err != nil {
			return nil, fmt.Errorf("get persoon claim Voornamen: %w", err)
		}

		naam.Voornamen = &data.Value

		if fields.Contains("naam.voorletters") {
			var letters string

			for _, name := range strings.Split(*naam.Voornamen, " ") {
				letters += fmt.Sprintf("%s.", name[0:1])
			}

			naam.Voorletters = &letters
		}
	}

	if fields.Contains("naam") || fields.Contains("naam.voorvoegsel") {
		var data struct {
			Value string `json:"value"`
		}
		if err := c.getPersoonClaim(ctx, id, "Voorvoegsel", &data); err != nil {
			return nil, fmt.Errorf("get persoon claim Voorvoegsel: %w", err)
		}

		if data.Value != "" {
			naam.Voorvoegsel = &data.Value
		}
	}

	if fields.Contains("naam") || fields.Contains("naam.geslachtsnaam") {
		var data struct {
			Value string `json:"value"`
		}
		if err := c.getPersoonClaim(ctx, id, "Geslachtsnaam", &data); err != nil {
			return nil, fmt.Errorf("get persoon claim Geslachtsnaam: %w", err)
		}

		naam.Geslachtsnaam = &data.Value
	}

	if fields.Contains("naam") || fields.Needs("naam.aanduidingNaamgebruik") {
		if data, err := c.getPersoonClaimNaamAanduidingNaamgebruik(ctx, id, fields); err != nil {
			if !errors.Is(err, ErrNotFound) {
				return naam, fmt.Errorf("get persoon claim naam aanduiding naamgebruik: %w", err)
			}
		} else {
			naam.AanduidingNaamgebruik = data
		}
	}

	// Compose the naam.VolledigeNaam field. IMPROVE: also support names with reverse Voornaam and Geslachtsnaam order? See e.g. https://en.wikipedia.org/wiki/Personal_name
	if fields.Contains("naam") || fields.Needs("naam.VolledigeNaam") {
		parts := make([]string, 3)
		if naam.Voornamen != nil && *naam.Voornamen != "" {
			parts = append(parts, *naam.Voornamen)
		}

		if naam.Voorvoegsel != nil && *naam.Voorvoegsel != "" {
			parts = append(parts, *naam.Voorvoegsel)
		}

		if naam.Geslachtsnaam != nil && *naam.Geslachtsnaam != "" {
			parts = append(parts, *naam.Geslachtsnaam)
		}

		str := strings.Join(parts, " ")
		naam.VolledigeNaam = &str
	}

	return naam, nil
}

func (c *Claim) getPersoonClaimNaamAanduidingNaamgebruik(ctx context.Context, id claimAPI.SubjectID, fields field.Fields) (*api.Waardetabel, error) {
	wt := &api.Waardetabel{}

	if fields.Contains("naam.aanduidingNaamgebruik.code") {
		if code, err := c.getPersoonClaimNaamAanduidingNaamgebruikCode(ctx, id); err != nil {
			return nil, fmt.Errorf("get persoon claim naam aanduiding naamgebruik code: %w", err)
		} else {
			wt.Code = code
		}
	}

	return wt, nil
}

func (c *Claim) getPersoonClaimNaamAanduidingNaamgebruikCode(ctx context.Context, id claimAPI.SubjectID) (*string, error) {
	var data struct {
		Value string `json:"value"`
	}
	if err := c.getPersoonClaim(ctx, id, "Geslachtsnaam", &data); err != nil {
		return nil, fmt.Errorf("get persoon claim Geslachtsnaam: %w", err)
	}

	return &data.Value, nil // IMPROVE: fetch/return something else?
}

func (c *Claim) getPersoonClaimGeboorte(ctx context.Context, id claimAPI.SubjectID, fields field.Fields) (geboorte *api.Geboorte, leeftijd *api.Leeftijd, err error) {
	if fields.Needs("geboorte") {
		geboorte = &api.Geboorte{}
	}

	if fields.Needs("geboorte.datum") || fields.Contains("leeftijd") {
		var date *Date
		if date, err = c.getPersoonClaimGeboorteDatum(ctx, id); err != nil {
			err = fmt.Errorf("get persoon claim Geboortedatum: %w", err)
			// Note: do not return, since e.g. Geboorteplaats might exist
		} else {
			t := time.Time(*date)

			if fields.Contains("geboorte.datum") {
				datum := &api.AbstractDatum{
					LangFormaat: monday.Format(t, "2 January 2006", monday.LocaleNlNL),
				}
				datum.FromVolledigeDatum(api.VolledigeDatum{
					Datum: types.Date{
						Time: t,
					},
				})

				geboorte.Datum = datum
			}

			if fields.Contains("leeftijd") {
				l := api.Leeftijd(age(t))
				leeftijd = &l
			}
		}
	}

	if fields.Needs("geboorte.plaats") {
		var geboorteplaats struct {
			Code         string `json:"code"`
			Omschrijving string `json:"omschrijving"`
		}
		if err = c.getPersoonClaim(ctx, id, "Geboorteplaats", &geboorteplaats); err != nil {
			err = fmt.Errorf("get persoon claim Geboorteplaats: %w", err)
			// Note: do not return, since Geboorteland might exist
		} else {
			geboorte.Plaats = &api.Waardetabel{
				Code:         &geboorteplaats.Code,
				Omschrijving: &geboorteplaats.Omschrijving,
			}
		}
	}

	if fields.Needs("geboorte.land") {
		var geboorteland struct {
			Code         string `json:"code"`
			Omschrijving string `json:"omschrijving"`
		}
		if err = c.getPersoonClaim(ctx, id, "Geboorteland", &geboorteland); err != nil {
			err = fmt.Errorf("get persoon claim Geboorteland: %w", err)
			return
		}

		geboorte.Land = &api.Waardetabel{
			Code:         &geboorteland.Code,
			Omschrijving: &geboorteland.Omschrijving,
		}
	}

	return
}

func age(t time.Time) int {
	now := time.Now()
	years := now.Year() - t.Year()

	if now.Month() < t.Month() {
		years--
	} else if now.Month() == t.Month() {
		if now.Day() < t.Day() {
			years--
		}
	}

	return years
}

func (c *Claim) getPersoonClaimGeboorteDatum(ctx context.Context, id claimAPI.SubjectID) (*Date, error) {
	var data struct {
		Value Date `json:"value"`
	}
	if err := c.getPersoonClaim(ctx, id, "Geboortedatum", &data); err != nil {
		return nil, fmt.Errorf("get persoon claim: %w", err)
	}

	return &data.Value, nil
}

func (c *Claim) getPersoonClaimOverlijden(ctx context.Context, id claimAPI.SubjectID, fields field.Fields) (*api.Overlijden, error) {
	overlijden := &api.Overlijden{}

	if fields.Contains("overlijden.datum") {
		date, err := c.getPersoonClaimOverlijdenDatum(ctx, id)
		if err != nil {
			return nil, fmt.Errorf("get persoon claim overlijdensdatum: %w", err)
		}

		if date != nil {
			t := time.Time(*date)

			datum := &api.AbstractDatum{
				LangFormaat: monday.Format(t, "2 January 2006", monday.LocaleNlNL),
			}
			datum.FromVolledigeDatum(api.VolledigeDatum{
				Datum: types.Date{
					Time: t,
				},
			})

			overlijden.Datum = datum
		}
	}

	return overlijden, nil
}

func (c *Claim) getPersoonClaimOverlijdenDatum(ctx context.Context, id claimAPI.SubjectID) (*Date, error) {
	var data struct {
		Value Date `json:"value"`
	}

	// if err := c.getPersoonClaim(ctx, id, claimAPI.Geboortedatum, &data); err != nil { // TODO: change to Overlijdensdatum when implemented
	// 	return nil, fmt.Errorf("get persoon claim: %w", err)
	// }

	if time.Time(data.Value).IsZero() {
		return nil, nil
	}

	return &data.Value, nil
}
