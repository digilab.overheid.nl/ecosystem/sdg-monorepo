package static

import (
	"context"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"reflect"

	"github.com/blevesearch/bleve"
	api "gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-backend/config"
)

type Static struct {
	cfg   config.RepositoryStaticConfig
	index bleve.Index
}

func New(cfg config.RepositoryStaticConfig) (*Static, error) {
	index, err := bleve.Open("example.bleve")
	if err != nil {
		if errors.Is(err, bleve.ErrorIndexPathDoesNotExist) {
			mapping := bleve.NewIndexMapping()
			index, err = bleve.New("example.bleve", mapping)
			if err != nil {
				return nil, fmt.Errorf("bleve new failed: %w", err)
			}

			if err := loadFile(index, cfg.DatasetPath); err != nil {
				return nil, fmt.Errorf("load file failed: %w", err)
			}

		} else {
			return nil, fmt.Errorf("bleve open failed: %w", err)
		}
	}

	return &Static{
		cfg:   cfg,
		index: index,
	}, nil
}

func loadFile(index bleve.Index, path string) error {
	file, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("could not open file: %w", err)
	}

	r := csv.NewReader(file)

	record, err := r.Read()
	if err != nil {
		log.Fatal(err)
	}

	headers, err := parseHeader(record)
	if err != nil {
		log.Fatal(err)
	}

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatal(err)
		}

		persoon, err := parseRecord(headers, record)
		if err != nil {
			if errors.Is(err, errInvalidRow) {
				continue
			}

			return fmt.Errorf("parse record failed: %w", err)
		}
		if err := index.Index(*persoon.ANummer, *persoon); err != nil {
			return fmt.Errorf("could not index %v: %w", *persoon.Burgerservicenummer, err)
		}
	}

	return nil
}

var errInvalidRow = errors.New("invalid row")

type header string

const (
	ANummerPersoon           header = "01.01.10"
	Burgerservicenummer      header = "01.01.20"
	Voornamen                header = "01.02.10"
	AdellijkeTitelPerdikaat  header = "01.02.20"
	VoorvoegselGeslachtsnaam header = "01.02.30"
	Geslachtsnaam            header = "01.02.40"
	Geboortedatum            header = "01.03.10"
	Geboorteplaats           header = "01.03.20"
	Geboorteland             header = "01.03.30"
	Geslachtsaanduiding      header = "01.04.10"
	AanduidingNaamgebruik    header = "01.61.10"
	IndicatieGeheim          header = "07.70.10"
	GemeenteVanInschrijving  header = "08.09.10"
)

var headers = []header{
	ANummerPersoon,
	Burgerservicenummer,
	Voornamen,
	AdellijkeTitelPerdikaat,
	VoorvoegselGeslachtsnaam,
	Geslachtsnaam,
	Geboortedatum,
	Geboorteplaats,
	Geboorteland,
	Geslachtsaanduiding,
	AanduidingNaamgebruik,
	GemeenteVanInschrijving,
}

func parseHeader(record []string) (map[header]int, error) {
	headers := map[header]int{}
	for index, value := range record {
		headers[header(value)] = index
	}

	return headers, nil
}

func parseRecord(headers map[header]int, record []string) (*api.Persoon, error) {
	if record[0] == "" {
		return nil, errInvalidRow
	}

	persoon := &api.Persoon{
		ANummer: toPtr(record[headers[ANummerPersoon]]),
		Adressering: &api.Adressering{
			Aanhef: new(string),
			Aanschrijfwijze: &api.Aanschrijfwijze{
				Aanspreekvorm: new(string),
				Naam:          new(string),
			},
			Adresregel1:                              new(string),
			Adresregel2:                              new(string),
			Adresregel3:                              new(string),
			GebruikInLopendeTekst:                    new(string),
			InOnderzoek:                              &api.AdresseringInOnderzoek{},
			IndicatieVastgesteldVerblijftNietOpAdres: new(bool),
			Land:                                     &api.Waardetabel{},
		},
		Burgerservicenummer:         toPtr(record[headers[Burgerservicenummer]]),
		DatumEersteInschrijvingGBA:  &api.AbstractDatum{},
		DatumInschrijvingInGemeente: &api.AbstractDatum{},
		EuropeesKiesrecht:           &api.EuropeesKiesrecht{},
		Geboorte: &api.Geboorte{
			Datum: &api.AbstractDatum{
				LangFormaat: record[headers[Geboortedatum]],
			},
			Land: &api.Waardetabel{
				Code: toPtr(record[headers[Geboorteland]]),
			},
			Plaats: &api.Waardetabel{
				Code: toPtr(record[headers[Geboorteplaats]]),
			},
		},
		// GeheimhoudingPersoonsgegevens: toPtr(record[headers[IndicatieGeheim]]),
		GemeenteVanInschrijving: &api.Waardetabel{
			Code: toPtr(record[headers[GemeenteVanInschrijving]]),
		},
		Geslacht: &api.Waardetabel{
			Code: toPtr(record[10]),
		},
		Immigratie:                 &api.Immigratie{},
		InOnderzoek:                &api.PersoonInOnderzoek{},
		IndicatieCurateleRegister:  new(bool),
		IndicatieGezagMinderjarige: &api.Waardetabel{},
		Kinderen:                   &[]api.Kind{},
		Leeftijd:                   new(int),
		Naam: &api.NaamPersoon{
			AanduidingNaamgebruik: &api.Waardetabel{
				Code: toPtr(record[13]),
			},
			AdellijkeTitelPredicaat: &api.AdellijkeTitelPredicaatType{
				Code: &record[headers[AdellijkeTitelPerdikaat]],
			},
			Geslachtsnaam: &record[headers[Geslachtsnaam]],
			InOnderzoek:   &api.NaamPersoonInOnderzoek{},
			VolledigeNaam: new(string),
			Voorletters:   new(string),
			Voornamen:     &record[headers[Voornamen]],
			Voorvoegsel:   &record[headers[VoorvoegselGeslachtsnaam]],
		},
		Nationaliteiten:       &[]api.AbstractNationaliteit{},
		OpschortingBijhouding: &api.OpschortingBijhouding{},
		Ouders:                &[]api.Ouder{},
		Overlijden:            &api.Overlijden{},
		Partners:              &[]api.Partner{},
		Rni:                   &[]api.RniDeelnemer{},
		UitsluitingKiesrecht:  &api.UitsluitingKiesrecht{},
		Verblijfplaats: &api.AbstractVerblijfplaats{
			Type: "",
		},
		Verblijfstitel: &api.Verblijfstitel{},
		Verificatie:    &api.Verificatie{},
	}

	return persoon, nil
}

func toPtr[T any](s T) *T {
	v := reflect.ValueOf(s)
	if !v.IsValid() {
		return nil
	}

	return &s
}

// RaadpleegMetBurgerservicenummer implements repository.Repository.
func (s *Static) RaadpleegMetBurgerservicenummer(ctx context.Context, request api.RaadpleegMetBurgerservicenummer) ([]api.Persoon, error) {
	query := bleve.NewMatchQuery("999990469")
	query.SetField("burgerservicenummer")

	searchRequest := bleve.NewSearchRequest(query)
	searchRequest.Fields = []string{"*"}
	searchResult, err := s.index.Search(searchRequest)
	if err != nil {
		return nil, fmt.Errorf("index search failed: %w", err)
	}

	personen := make([]api.Persoon, 0, len(searchResult.Hits))
	for _, result := range searchResult.Hits {
		persoon := api.Persoon{
			ANummer:                       getString("aNummer", result.Fields),
			Adressering:                   &api.Adressering{},
			Burgerservicenummer:           getString("burgerservicenummer", result.Fields),
			DatumEersteInschrijvingGBA:    &api.AbstractDatum{},
			DatumInschrijvingInGemeente:   &api.AbstractDatum{},
			EuropeesKiesrecht:             &api.EuropeesKiesrecht{},
			Geboorte:                      &api.Geboorte{},
			GeheimhoudingPersoonsgegevens: new(bool),
			GemeenteVanInschrijving:       &api.Waardetabel{},
			Geslacht:                      &api.Waardetabel{},
			Immigratie:                    &api.Immigratie{},
			InOnderzoek:                   &api.PersoonInOnderzoek{},
			IndicatieCurateleRegister:     getBool("indicatieCurateleRegister", result.Fields),
			IndicatieGezagMinderjarige:    &api.Waardetabel{},
			Kinderen:                      &[]api.Kind{},
			Leeftijd:                      new(int),
			Naam: &api.NaamPersoon{
				AanduidingNaamgebruik:   &api.Waardetabel{},
				AdellijkeTitelPredicaat: &api.AdellijkeTitelPredicaatType{},
				Geslachtsnaam:           getString("geslachtsnaam", result.Fields),
				InOnderzoek:             &api.NaamPersoonInOnderzoek{},
				VolledigeNaam:           new(string),
				Voorletters:             new(string),
				Voornamen:               getString("voornamen", result.Fields),
				Voorvoegsel:             getString("voorvoegsel", result.Fields),
			},
			Nationaliteiten:       &[]api.AbstractNationaliteit{},
			OpschortingBijhouding: &api.OpschortingBijhouding{},
			Ouders:                &[]api.Ouder{},
			Overlijden:            &api.Overlijden{},
			Partners:              &[]api.Partner{},
			Rni:                   &[]api.RniDeelnemer{},
			UitsluitingKiesrecht:  &api.UitsluitingKiesrecht{},
			Verblijfplaats:        &api.AbstractVerblijfplaats{},
			Verblijfstitel:        &api.Verblijfstitel{},
			Verificatie:           &api.Verificatie{},
		}

		personen = append(personen, persoon)
	}

	return personen, nil
}

func getString(name string, fields map[string]any) *string {
	field := getField(name, fields)
	if field == nil {
		return nil
	}

	value, ok := field.(string)
	if !ok {
		return nil
	}
	return &value
}

func getBool(name string, fields map[string]any) *bool {
	field := getField(name, fields)
	if field == nil {
		return nil
	}

	value, ok := field.(bool)
	if !ok {
		return nil
	}

	return &value
}

func getField(name string, fields map[string]any) any {
	v, ok := fields[name]
	if !ok {
		return nil
	}

	return v
}

// ZoekMetGeslachtsnaamEnGeboorteDatum implements repository.Repository.
func (s *Static) ZoekMetGeslachtsnaamEnGeboorteDatum(ctx context.Context, params api.ZoekMetGeslachtsnaamEnGeboortedatum) ([]api.PersoonBeperkt, error) {
	panic("unimplemented")
}

// ZoekMetNaamEnGemeenteVanInschrijving implements repository.Repository.
func (s *Static) ZoekMetNaamEnGemeenteVanInschrijving(ctx context.Context, params api.ZoekMetNaamEnGemeenteVanInschrijving) ([]api.PersoonBeperkt, error) {
	panic("unimplemented")
}

// ZoekMetNummeraanduidingIdentificatie implements repository.Repository.
func (s *Static) ZoekMetNummeraanduidingIdentificatie(ctx context.Context, params api.ZoekMetNummeraanduidingIdentificatie) ([]api.PersoonBeperkt, error) {
	panic("unimplemented")
}

// ZoekMetPostcodeEnHuisnummer implements repository.Repository.
func (s *Static) ZoekMetPostcodeEnHuisnummer(ctx context.Context, params api.ZoekMetPostcodeEnHuisnummer) ([]api.PersoonBeperkt, error) {
	panic("unimplemented")
}

// ZoekMetStraatHuisnummerEnGemeenteVanInschrijving implements repository.Repository.
func (s *Static) ZoekMetStraatHuisnummerEnGemeenteVanInschrijving(ctx context.Context, params api.ZoekMetStraatHuisnummerEnGemeenteVanInschrijving) ([]api.PersoonBeperkt, error) {
	panic("unimplemented")
}
