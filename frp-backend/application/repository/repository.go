package repository

import (
	"context"

	api "gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-api"
)

type Repository interface {
	RaadpleegAllePersonen(ctx context.Context, params api.GetPersonen) (*api.GetPersonenQueryResponse, error)
	RaadpleegMetBurgerservicenummer(ctx context.Context, params api.RaadpleegMetBurgerservicenummer) ([]api.Persoon, error)
	ZoekMetGeslachtsnaamEnGeboorteDatum(ctx context.Context, params api.ZoekMetGeslachtsnaamEnGeboortedatum) ([]api.PersoonBeperkt, error)
	ZoekMetNaamEnGemeenteVanInschrijving(ctx context.Context, params api.ZoekMetNaamEnGemeenteVanInschrijving) ([]api.PersoonBeperkt, error)
	ZoekMetNummeraanduidingIdentificatie(ctx context.Context, params api.ZoekMetNummeraanduidingIdentificatie) ([]api.PersoonBeperkt, error)
	ZoekMetPostcodeEnHuisnummer(ctx context.Context, params api.ZoekMetPostcodeEnHuisnummer) ([]api.PersoonBeperkt, error)
	ZoekMetStraatHuisnummerEnGemeenteVanInschrijving(ctx context.Context, params api.ZoekMetStraatHuisnummerEnGemeenteVanInschrijving) ([]api.PersoonBeperkt, error)
}
