package application

import (
	"context"
	"log/slog"
	"net/http"
	"strings"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"

	api "gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-backend/application/repository"
	"gitlab.com/digilab.overheid.nl/ecosystem/sdg-monorepo/frp-backend/config"
)

type Application struct {
	*http.Server
	logger     *slog.Logger
	repository repository.Repository
	cfg        *config.Config
}

// GetPersonen implements api.StrictServerInterface.
func (app *Application) GetPersonen(ctx context.Context, request api.GetPersonenRequestObject) (api.GetPersonenResponseObject, error) {
	var response api.GetPersonenResponseObject

	body, err := app.repository.RaadpleegAllePersonen(ctx, *request.Body)
	if err != nil {
		return response, err
	}

	return api.GetPersonen200JSONResponse{
		Body:    *body,
		Headers: api.GetPersonen200ResponseHeaders{},
	}, nil
}

// Personen implements api.StrictServerInterface.
func (app *Application) Personen(ctx context.Context, request api.PersonenRequestObject) (api.PersonenResponseObject, error) {
	var response api.PersonenQueryResponse

	discriminator, err := request.Body.Discriminator()
	if err != nil {
		return nil, err
	}

	switch strings.ToLower(discriminator) {
	case "raadpleegmetburgerservicenummer":
		response, err = app.raadpleegMetBurgerservicenummer(ctx, request.Body)
		if err != nil {
			return nil, err
		}
	case "zoekmetgeslachtsnaamengeboortedatum":
		response, err = app.zoekMetGeslachtsnaamEnGeboortedatum(ctx, request.Body)
		if err != nil {
			return nil, err
		}
	case "zoekmetnaamengemeentevaninschrijving":
		response, err = app.zoekMetNaamEnGemeenteVanInschrijving(ctx, request.Body)
		if err != nil {
			return nil, err
		}
	case "zoekmetnummeraanduidingidentificatie":
		response, err = app.zoekMetNaamEnGemeenteVanInschrijving(ctx, request.Body)
		if err != nil {
			return nil, err
		}
	case "zoekmetpostcodeenhuisnummer":
		response, err = app.ZoekMetPostcodeEnHuisnummer(ctx, request.Body)
		if err != nil {
			return nil, err
		}
	case "zoekmetstraathuisnummerengemeentevaninschrijving":
		response, err = app.zoekMetStraatHuisnummerEnGemeenteVanInschrijving(ctx, request.Body)
		if err != nil {
			return nil, err
		}
	}

	return api.Personen200JSONResponse{
		Body:    response,
		Headers: api.Personen200ResponseHeaders{},
	}, nil
}

func (app *Application) raadpleegMetBurgerservicenummer(ctx context.Context, request *api.PersonenJSONRequestBody) (api.PersonenQueryResponse, error) {
	var response api.PersonenQueryResponse

	body, err := request.AsRaadpleegMetBurgerservicenummer()
	if err != nil {
		return response, err
	}

	personen, err := app.repository.RaadpleegMetBurgerservicenummer(ctx, body)
	if err != nil {
		return response, err
	}

	params := &api.RaadpleegMetBurgerservicenummerResponse{
		Personen: &personen,
	}

	if err := response.FromRaadpleegMetBurgerservicenummerResponse(*params); err != nil {
		return response, err
	}

	return response, nil
}

func (app *Application) zoekMetGeslachtsnaamEnGeboortedatum(ctx context.Context, request *api.PersonenJSONRequestBody) (api.PersonenQueryResponse, error) {
	var response api.PersonenQueryResponse

	body, err := request.AsZoekMetGeslachtsnaamEnGeboortedatum()
	if err != nil {
		return response, err
	}

	personen, err := app.repository.ZoekMetGeslachtsnaamEnGeboorteDatum(ctx, body)
	if err != nil {
		return response, err
	}

	params := &api.ZoekMetGeslachtsnaamEnGeboortedatumResponse{
		Personen: &personen,
	}

	if err := response.FromZoekMetGeslachtsnaamEnGeboortedatumResponse(*params); err != nil {
		return response, err
	}

	return response, nil
}

func (app *Application) zoekMetNaamEnGemeenteVanInschrijving(ctx context.Context, request *api.PersonenJSONRequestBody) (api.PersonenQueryResponse, error) {
	var response api.PersonenQueryResponse

	body, err := request.AsZoekMetNaamEnGemeenteVanInschrijving()
	if err != nil {
		return response, err
	}

	personen, err := app.repository.ZoekMetNaamEnGemeenteVanInschrijving(ctx, body)
	if err != nil {
		return response, err
	}

	params := &api.ZoekMetNaamEnGemeenteVanInschrijvingResponse{
		Personen: &personen,
	}

	if err := response.FromZoekMetNaamEnGemeenteVanInschrijvingResponse(*params); err != nil {
		return response, err
	}

	return response, nil
}

func (app *Application) zoekMetNummeraanduidingIdentificatie(ctx context.Context, request *api.PersonenJSONRequestBody) (api.PersonenQueryResponse, error) {
	var response api.PersonenQueryResponse

	body, err := request.AsZoekMetNummeraanduidingIdentificatie()
	if err != nil {
		return response, err
	}

	personen, err := app.repository.ZoekMetNummeraanduidingIdentificatie(ctx, body)
	if err != nil {
		return response, err
	}

	params := &api.ZoekMetNummeraanduidingIdentificatieResponse{
		Personen: &personen,
	}

	if err := response.FromZoekMetNummeraanduidingIdentificatieResponse(*params); err != nil {
		return response, err
	}

	return response, nil
}

func (app *Application) ZoekMetPostcodeEnHuisnummer(ctx context.Context, request *api.PersonenJSONRequestBody) (api.PersonenQueryResponse, error) {
	var response api.PersonenQueryResponse

	body, err := request.AsZoekMetPostcodeEnHuisnummer()
	if err != nil {
		return response, err
	}

	personen, err := app.repository.ZoekMetPostcodeEnHuisnummer(ctx, body)
	if err != nil {
		return response, err
	}

	params := &api.ZoekMetPostcodeEnHuisnummerResponse{
		Personen: &personen,
	}

	if err := response.FromZoekMetPostcodeEnHuisnummerResponse(*params); err != nil {
		return response, err
	}

	return response, nil
}

func (app *Application) zoekMetStraatHuisnummerEnGemeenteVanInschrijving(ctx context.Context, request *api.PersonenJSONRequestBody) (api.PersonenQueryResponse, error) {
	var response api.PersonenQueryResponse

	body, err := request.AsZoekMetStraatHuisnummerEnGemeenteVanInschrijving()
	if err != nil {
		return response, err
	}

	personen, err := app.repository.ZoekMetStraatHuisnummerEnGemeenteVanInschrijving(ctx, body)
	if err != nil {
		return response, err
	}

	params := &api.ZoekMetStraatHuisnummerEnGemeenteVanInschrijvingResponse{
		Personen: &personen,
	}

	if err := response.FromZoekMetStraatHuisnummerEnGemeenteVanInschrijvingResponse(*params); err != nil {
		return response, err
	}

	return response, nil
}

func New(logger *slog.Logger, cfg *config.Config, repo repository.Repository) Application {
	app := Application{
		Server: &http.Server{
			Addr: cfg.ListenAddress,
		},
		logger:     logger,
		cfg:        cfg,
		repository: repo,
	}
	return app
}

func (app *Application) Router() {
	r := chi.NewRouter()

	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.Heartbeat("/healthz"))
	r.Use(middleware.SetHeader("Content-Type", "application/json"))

	apiHandler := api.NewStrictHandler(app, nil)
	r.Mount("/v0", api.Handler(apiHandler))

	r.Group(func(r chi.Router) {
		r.Use(middleware.SetHeader("Content-Type", "application/yaml"))
		r.Get("/openapi.yaml", app.OpenAPI)
	})

	app.Handler = r
}

func (app *Application) ListenAndServe() error {
	app.logger.Info("starting server", "listenaddress", app.cfg.ListenAddress)
	return app.Server.ListenAndServe()
}
