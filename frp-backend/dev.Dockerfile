FROM digilabpublic.azurecr.io/golang:1.22.2-alpine3.19 as builder

# Cache dependencies
RUN ["go", "install", "github.com/githubnemo/CompileDaemon@latest"]

COPY frp-api/go.mod frp-api/go.sum /build/frp-api/
COPY claim-api/go.mod claim-api/go.sum /build/claim-api/

WORKDIR /build/frp-backend
COPY frp-backend/go.mod frp-backend/go.sum ./

RUN go mod download

## Build the Go Files
COPY frp-api /build/frp-api
COPY claim-api /build/claim-api

WORKDIR /build/frp-backend
COPY frp-backend ./

## Run the server for dev
ENTRYPOINT CompileDaemon -log-prefix=false -build="go build -o server ." -command="./server serve"
